'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _objectDestructuringEmpty(obj) { if (obj == null) throw new TypeError("Cannot destructure undefined"); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Bill = function () {
	function Bill() {
		_classCallCheck(this, Bill);

		this.selectors = {
			el: '.js-bill',
			shade: '.js-bill-shade',
			btn: '.js-bill-btn',
			form: '.js-bill-form',
			close: '.js-bill-close',
			text: '.js-bill-text',
			link: '.js-bill-link',
			amount: '.js-bill-amount'
		};

		this.classes = {
			show: 'bill_show',
			answer: 'bill_answer',
			fixed: 'bill_fixed'
		};

		this.data = {};

		this.el = $(this.selectors.el);
		this.$window = $(window);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data).init();
		}
	}

	_createClass(Bill, [{
		key: 'get',
		value: function get(_ref, _ref2) {
			var shade = _ref.shade,
			    btn = _ref.btn,
			    form = _ref.form,
			    close = _ref.close,
			    text = _ref.text,
			    link = _ref.link,
			    amount = _ref.amount;

			_objectDestructuringEmpty(_ref2);

			this.elems = this.el.toArray().map(function (el) {
				var $el = $(el);
				return {
					$el: $el,
					$shade: $el.find(shade),
					$btn: $el.find(btn),
					$form: $el.find(form),
					$close: $el.find(close),
					$text: $el.find(text),
					$link: $el.find(link),
					$amount: $el.find(amount),
					data: {}
				};
			});
			return this;
		}
	}, {
		key: 'init',
		value: function init() {
			var _this = this;

			this.elems.forEach(function (el) {
				_this.$window.on('bill:show', { that: _this, el: el }, _this.toggleForm).on('bill:amount', { that: _this, el: el }, _this.setAmount);
				el.$shade.on('click', { that: _this, el: el }, _this.toggleForm);
				el.$close.on('click', { that: _this, el: el }, _this.toggleForm);
				el.$btn.on('click', { that: _this, el: el }, _this.validateForm);
			});
		}
	}, {
		key: 'scroll',
		value: function scroll(e) {
			var _e$data = e.data,
			    that = _e$data.that,
			    el = _e$data.el,
			    fixed = that.classes.fixed;


			el.$el.toggleClass(fixed, window.scrollY > 40);
		}
	}, {
		key: 'setAmount',
		value: function setAmount(e, data) {
			var _e$data2 = e.data,
			    that = _e$data2.that,
			    el = _e$data2.el;


			el.$amount.val(data.value);
		}
	}, {
		key: 'toggleForm',
		value: function toggleForm(e) {
			var _e$data3 = e.data,
			    that = _e$data3.that,
			    el = _e$data3.el,
			    show = that.classes.show;


			if (el.$el.hasClass(show)) {
				that.$window.off('scroll', { that: that, el: el }, that.scroll);
			} else {
				that.$window.on('scroll', { that: that, el: el }, that.scroll);
				that.scroll({ data: { el: el, that: that } });
			}

			el.$el.toggleClass(show);
		}
	}, {
		key: 'validateForm',
		value: function validateForm(e) {
			e.preventDefault();

			var _e$data4 = e.data,
			    that = _e$data4.that,
			    el = _e$data4.el;

			var valid = true;

			el.$form.find('.js-field').each(function (i, item) {
				var $el = $(item);
				$el.trigger('field:validate');
				if ($el.data('error') === true) {
					valid = false;
				}
			});

			if (valid) {
				that.sendForm(el);
			} else {
				console.log('Form is not valid.');
			}
		}
	}, {
		key: 'sendForm',
		value: function sendForm(el) {
			var $form = el.$form,
			    url = $form.prop('action'),
			    data = $form.serialize(),
			    callback = this.result,
			    context = this;

			var additional = $.param(el.$form.data());

			this.$window.trigger('ajax:get', { url: url, data: data + '&' + additional, callback: callback, context: { that: this, el: el } });
		}
	}, {
		key: 'result',
		value: function result(_ref3) {
			var data = _ref3.data,
			    context = _ref3.context;
			var that = context.that,
			    el = context.el;


			if (data.success) {
				el.$text.text(data.text);
				el.$link.html(data.link);
				el.$el.addClass(that.classes.answer);
			} else {
				console.log('error');
			}
		}
	}]);

	return Bill;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var BlogPage = function () {
	function BlogPage() {
		_classCallCheck(this, BlogPage);

		this.selectors = {
			post: '.js-blog-page',
			picture: '.js-picture'
		};

		this.el = $(this.selectors.post);

		if (this.el.length) {
			this.init();
		}
	}

	_createClass(BlogPage, [{
		key: 'init',
		value: function init() {
			var pictures = Array.from(document.querySelectorAll(this.selectors.picture));

			pictures.forEach(function (picture) {
				if (picture.firstChild.naturalWidth < 800) {
					picture.classList.add('blog-page__picture_small');
				}
			});
		}
	}]);

	return BlogPage;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Bonuses = function () {
	function Bonuses() {
		_classCallCheck(this, Bonuses);

		this.selectors = {
			el: '.js-bonuses',
			total: '.js-total',
			hot: '.js-hot',
			btn: '.js-button',
			used: '.js-used'
		};

		this.classes = {};

		this.data = {
			total: 'total',
			percent: 'percent',
			possible: 'possible',
			eUse: 'event-use',
			eCartUpdate: 'event-cart-update'
		};

		this.el = $(this.selectors.el);
		this.$window = $(window);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data).init();
		}
	}

	_createClass(Bonuses, [{
		key: 'get',
		value: function get(_ref, data) {
			var total = _ref.total,
			    hot = _ref.hot,
			    btn = _ref.btn,
			    used = _ref.used;

			this.elems = this.el.toArray().map(function (el) {
				var $el = $(el);
				return {
					$el: $el,
					$total: $el.find(total),
					$hot: $el.find(hot),
					$btn: $el.find(btn),
					$used: $el.find(used),
					data: {
						eUse: $el.data(data.eUse) || 'cart:bonuses',
						total: $el.data(data.total),
						eCartUpdate: $el.data(data.eCartUpdate) || 'cart:update',
						percentOfSumm: $el.data(data.percent) || 30,
						possible: $el.data(data.possible) || 0,
						used: false
					}
				};
			});
			return this;
		}
	}, {
		key: 'init',
		value: function init() {
			var _this = this;

			this.elems.forEach(function (el) {
				el.$btn.on('click', { that: _this, el: el }, _this.use);

				_this.$window.on(el.data.eCartUpdate, { that: _this, el: el }, _this.setBonuses);
			});
		}
	}, {
		key: 'setBonuses',
		value: function setBonuses(e, data) {
			var _e$data = e.data,
			    that = _e$data.that,
			    el = _e$data.el,
			    summ = data.TotalItemPrice,
			    _el$data = el.data,
			    total = _el$data.total,
			    used = _el$data.used,
			    percent = _el$data.percentOfSumm;


			var persOfSumm = (summ / 100 * percent).toFixed(),
			    possible = total < persOfSumm ? total : persOfSumm;

			that.btnText(el);

			el.data.possible = possible;
			if (used) {
				el.$total.text(total - possible);
			} else {
				el.$total.text(total);
			}
			that.btnText(el);
		}
	}, {
		key: 'btnText',
		value: function btnText(el) {
			var _el$data2 = el.data,
			    used = _el$data2.used,
			    possible = _el$data2.possible,
			    text = used ? 'Вернуть' : 'Использовать';


			el.$btn.text(text + ' ' + possible + ' \u0431\u0430\u043B\u043B\u043E\u0432');
		}
	}, {
		key: 'use',
		value: function use(e) {
			e.preventDefault();
			var _e$data2 = e.data,
			    that = _e$data2.that,
			    el = _e$data2.el,
			    total = el.data.total;

			var used = 0;

			if (!el.data.used) {
				used = el.data.possible;
				el.$total.text(total - el.data.possible);
				el.data.used = true;
			} else {
				used = 0;
				el.$total.text(total);
				el.data.used = false;
			}

			el.$used.val(used);
			that.btnText(el);

			that.$window.trigger(el.data.eUse, { used: used });
		}
	}]);

	return Bonuses;
}();
"use strict";
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var CartItem = function () {
	function CartItem() {
		_classCallCheck(this, CartItem);

		this.selectors = {
			el: '.js-cart-item',
			counter: '.js-counter',
			del: '.js-delete',
			return: '.js-return',
			add: '.js-add',
			price: '.js-price',
			form: '.js-cart-item-form'
		};

		this.classes = {
			deleted: 'cart-item_deleted',
			offer: 'cart-item_offer'
		};

		this.data = {
			id: 'id',
			price: 'item-price',
			weight: 'item-weight',
			amount: 'item-amount',
			eventChange: 'event-change',
			eventAdd: 'event-add',
			eventGet: 'event-get'
		};

		this.el = $(this.selectors.el);
		this.$window = $(window);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data).init();
		}
	}

	_createClass(CartItem, [{
		key: 'get',
		value: function get(_ref, data) {
			var _this = this;

			var counter = _ref.counter,
			    del = _ref.del,
			    ret = _ref.return,
			    add = _ref.add,
			    price = _ref.price,
			    form = _ref.form;
			var id = data.id,
			    dataPrice = data.price,
			    weight = data.weight,
			    amount = data.amount,
			    eventChange = data.eventChange,
			    eventAdd = data.eventAdd,
			    eventGet = data.eventGet;


			this.elems = this.el.toArray().map(function (el) {
				var $el = $(el);
				return {
					$el: $el,
					$counter: $el.find(counter),
					$del: $el.find(del),
					$return: $el.find(ret),
					$add: $el.find(add),
					$price: $el.find(price),
					$form: $el.find(form),
					data: {
						id: $el.data(id),
						price: $el.data(dataPrice),
						weight: $el.data(weight) || false,
						amount: $el.data(amount),
						eventChange: $el.data(eventChange) || 'cartItem:change',
						eventAdd: $el.data(eventAdd) || 'cartItem:add',
						eventGet: $el.data(eventGet) || 'cartItem:get',
						isDeleted: $el.hasClass(_this.classes.deleted)
					}
				};
			});
			return this;
		}
	}, {
		key: 'init',
		value: function init() {
			var _this2 = this;

			this.elems.forEach(function (el) {
				el.$del.on('click', { that: _this2, el: el }, _this2.deleteItem);
				el.$return.on('click', { that: _this2, el: el }, _this2.returnItem);
				el.$add.on('click', { that: _this2, el: el }, _this2.addItem);
				el.$counter.on('counter:change', { that: _this2, el: el }, _this2.amountChange);
				el.$el.on(el.data.eventGet, { el: el }, _this2.getItemData);
			});
		}
	}, {
		key: 'amountChange',
		value: function amountChange(e, data) {
			var _e$data = e.data,
			    that = _e$data.that,
			    el = _e$data.el,
			    _el$data = el.data,
			    eventChange = _el$data.eventChange,
			    id = _el$data.id,
			    price = _el$data.price,
			    weight = _el$data.weight,
			    value = data.value,
			    result = data.result,
			    formData = $.param(el.$form.data()) + '&' + el.$form.serialize(),
			    url = el.$form.prop('action'),
			    rate = 1000;


			el.data.amount = value;
			var summ = value * rate * (price * rate) / (rate * rate),
			    mass = weight ? weight * value : value;

			el.$price.text(summ.toString().replace('.', ','));

			el.$el.trigger(el.data.eventChange, { id: id, amount: value, summ: summ, mass: mass });

			that.$window.trigger('ajax:get', { url: url, data: formData, callback: that.cartСontents, context: that });
		}
	}, {
		key: 'addItem',
		value: function addItem(e) {
			var _e$data2 = e.data,
			    el = _e$data2.el,
			    that = _e$data2.that,
			    _el$data2 = el.data,
			    eventChange = _el$data2.eventChange,
			    id = _el$data2.id;


			el.$el.removeClass(that.classes.offer);
			el.$counter.trigger('counter:get');
			el.$el.trigger(el.data.eventAdd);
		}
	}, {
		key: 'deleteItem',
		value: function deleteItem(e) {
			var _e$data3 = e.data,
			    el = _e$data3.el,
			    that = _e$data3.that,
			    _el$data3 = el.data,
			    eventChange = _el$data3.eventChange,
			    id = _el$data3.id,
			    idName = el.$counter.find('input').prop('name'),
			    formData = $.param(el.$form.data()) + '&' + idName + '=0',
			    url = el.$form.prop('action');


			el.data.isDeleted = true;
			el.$el.addClass(that.classes.deleted);
			el.$el.trigger(el.data.eventChange, { id: id, amount: 0, summ: 0, mass: 0 });

			that.$window.trigger('ajax:get', { url: url, data: formData, callback: that.cartСontents, context: that });
		}
	}, {
		key: 'returnItem',
		value: function returnItem(e) {
			var _e$data4 = e.data,
			    el = _e$data4.el,
			    that = _e$data4.that;


			el.isDeleted = false;
			el.$el.removeClass(that.classes.deleted);
			el.$counter.trigger('counter:get');
		}
	}, {
		key: 'getItemData',
		value: function getItemData(e) {
			var el = e.data.el;


			el.$counter.trigger('counter:get');
		}
	}, {
		key: 'cart\u0421ontents',
		value: function cartOntents(_ref2) {
			var data = _ref2.data,
			    that = _ref2.context;

			that.$window.trigger('cart:update', data);
		}
	}]);

	return CartItem;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var CartPage = function () {
	function CartPage() {
		_classCallCheck(this, CartPage);

		this.selectors = {
			el: '.js-cart-page',
			itemsWrap: '.js-items',
			goods: '.js-cart-item',
			offers: '.js-offers',
			offTitle: '.js-offers-title',
			address: '#address',
			city: '.cart-page__form-city .js-switch',
			savedAddr: '.js-field-options',
			cityText: '.js-city-delivery',
			cityDate: '.js-city-date',
			form: '.js-cart-page-form',
			payment: '.js-payment-method',
			deliveryId: '.js-delivery-id',
			deliveryPrice: '.js-delivery-price',
			inMkad: '.js-in-mkad',
			mkadDist: '.js-mkad-dist',
			email: '.js-cart-page-email',
			bill: '.js-cert-page-bill'
		};

		this.data = {
			eventGoodsAdd: 'event-goods-add'
		};

		this.el = $(this.selectors.el);
		this.$window = $(window);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data).init();
		}
	}

	_createClass(CartPage, [{
		key: 'get',
		value: function get(_ref, data) {
			var itemsWrap = _ref.itemsWrap,
			    goods = _ref.goods,
			    offers = _ref.offers,
			    offTitle = _ref.offTitle,
			    address = _ref.address,
			    city = _ref.city,
			    savedAddr = _ref.savedAddr,
			    cityText = _ref.cityText,
			    cityDate = _ref.cityDate,
			    form = _ref.form,
			    payment = _ref.payment,
			    deliveryId = _ref.deliveryId,
			    deliveryPrice = _ref.deliveryPrice,
			    inMkad = _ref.inMkad,
			    mkadDist = _ref.mkadDist,
			    email = _ref.email,
			    bill = _ref.bill;
			var eventGoodsAdd = data.eventGoodsAdd;


			this.elems = this.el.toArray().map(function (el) {
				var $el = $(el);
				return {
					$el: $el,
					$itemsWrap: $el.find(itemsWrap),
					$goods: $el.find(goods),
					$offers: $el.find(offers),
					$offTitle: $el.find(offTitle),
					$address: $el.find(address),
					$city: $el.find(city),
					$savedAddr: $el.find(savedAddr),
					$cityText: $el.find(cityText),
					$cityDate: $el.find(cityDate),
					$form: $el.find(form),
					$payment: $el.find(payment),
					$deliveryId: $el.find(deliveryId),
					$deliveryPrice: $el.find(deliveryPrice),
					$inMkad: $el.find(inMkad),
					$mkadDist: $el.find(mkadDist),
					$email: $el.find(email),
					$bill: $el.find(bill),
					data: {
						eventGoodsAdd: $el.data(eventGoodsAdd) || 'cartItem:add',
						cityChanged: false,
						email: false,
						cityDateName: $el.find(cityDate).not(":hidden").find('.js-select-native').prop('name')
					}
				};
			});
			return this;
		}
	}, {
		key: 'init',
		value: function init() {
			var _this = this;

			this.$window.trigger('cart:dontShowList');

			this.elems.forEach(function (el) {
				el.$bill.on('click', { that: _this }, _this.showBill);

				el.$goods.on('cartItem:change', { that: _this, el: el }, _this.goodsChange).on(el.data.eventGoodsAdd, { that: _this, el: el }, _this.goodsAdd);

				el.$city.on('switch:change', { that: _this, el: el }, _this.setCity).trigger('switch:get');
				el.$savedAddr.on('field-option:change', { that: _this, el: el }, _this.reloadSuggestions);

				el.$form.sisyphus();

				_this.$window.on('order:payment', { that: _this, el: el }, _this.changePayment).on('cart:deliveryRecalc', { that: _this, el: el }, _this.deliveryRecalc).on('cart:getEmail', { that: _this, el: el }, _this.getEmail).on('cart:validate', { that: _this, el: el }, _this.validateForm);
			});
		}
	}, {
		key: 'showBill',
		value: function showBill(e) {
			var that = e.data.that;


			that.$window.trigger('bill:show');
		}
	}, {
		key: 'getEmail',
		value: function getEmail(e) {
			var _e$data = e.data,
			    that = _e$data.that,
			    el = _e$data.el;

			var email = false;

			el.$email.trigger('field:validate');
			if (el.$email.data('error') !== 'true') {
				email = el.$email.find('.js-field-input').val();
			}

			that.$window.trigger('cart:email', { email: email });
		}
	}, {
		key: 'validateForm',
		value: function validateForm(e) {
			var _e$data2 = e.data,
			    that = _e$data2.that,
			    el = _e$data2.el;

			var valid = true;

			el.$form.find('.js-field').each(function (i, item) {
				var $el = $(item);
				$el.trigger('field:validate');
				if ($el.data('error') === true) {
					valid = false;
				}
			});

			if (el.data.deliveryIsValid === undefined) {
				el.$savedAddr.trigger('field-option:validate');
			}

			if (valid && el.data.deliveryIsValid) {
				that.$window.trigger('cart:submit');
				// el.$form.serialize();
				el.$form.submit();
			} else {
				console.log('Form is not valid.');
			}
		}
	}, {
		key: 'deliveryRecalc',
		value: function deliveryRecalc(e) {
			var _e$data3 = e.data,
			    that = _e$data3.that,
			    el = _e$data3.el;


			if (el.$address.val() !== '') {
				that.getDeliveryPrice(el);
			}
		}
	}, {
		key: 'changePayment',
		value: function changePayment(e, data) {
			var _e$data4 = e.data,
			    that = _e$data4.that,
			    el = _e$data4.el,
			    value = data.value;


			el.$payment.val(value);
		}
	}, {
		key: 'setCity',
		value: function setCity(e, data) {
			var _e$data5 = e.data,
			    that = _e$data5.that,
			    el = _e$data5.el,
			    _data$data = data.data,
			    kladr = _data$data.kladr,
			    id = _data$data.id;


			that.showCityData({ that: that, el: el, id: id });

			var constraints = kladr.split(',').map(function (val) {
				return {
					locations: { kladr_id: val.replace(' ', '') }
				};
			});

			if (!el.data.cityChanged) {
				el.$savedAddr.find('.js-field').trigger('field:empty');
			} else {
				el.data.cityChanged = false;
			}

			el.data.suggestions = el.$address.suggestions({
				token: "670831253184a6570dcfb29298d37c4800de6d75",
				type: "ADDRESS",
				constraints: constraints,
				onSelect: function onSelect(suggestion) {
					console.log(suggestion);
					that.getDeliveryPrice(el);
				}
			}).suggestions();
		}
	}, {
		key: 'showCityData',
		value: function showCityData(_ref2) {
			var that = _ref2.that,
			    el = _ref2.el,
			    id = _ref2.id;

			el.$cityText.each(function (i, elem) {
				var $el = $(elem);
				if (i == id) {
					$el.show();
				} else {
					$el.hide();
				}
			});
			el.$cityDate.each(function (i, elem) {
				var $el = $(elem);
				if (i == id) {
					$el.show().find('.js-select-native').prop('name', el.data.cityDateName);
				} else {
					$el.hide().find('.js-select-native').prop('name', '');
				}
			});
		}
	}, {
		key: 'reloadSuggestions',
		value: function reloadSuggestions(e, _ref3) {
			var data = _ref3.data;
			var _e$data6 = e.data,
			    el = _e$data6.el,
			    that = _e$data6.that;

			var id = void 0;

			if (!data.verified) {
				el.data.suggestions.update();
			} else {
				if (data.city == 'spb') {
					id = 1;
				} else if (data.city == 'moscow') {
					id = 0;
				}
				el.data.cityChanged = true;
				el.$city.trigger('switch:update', { id: id });
				that.getDeliveryPrice(el);
			}
		}
	}, {
		key: 'getDeliveryPrice',
		value: function getDeliveryPrice(el) {
			var data = el.$form.serialize(),
			    url = el.$form.data('url-delivery'),
			    ajaxMethod = el.$form.data('ajax-method') || 'POST',
			    callback = this.deliveryPrice;

			this.$window.trigger('ajax:get', { url: url, type: ajaxMethod, data: data, callback: callback, context: { that: this, el: el } });
		}
	}, {
		key: 'deliveryPrice',
		value: function deliveryPrice(_ref4) {
			var data = _ref4.data,
			    context = _ref4.context;
			var inMKAD = data.inMKAD,
			    MkadDistance = data.MkadDistance,
			    price = data.price,
			    id = data.id,
			    error = data.error,
			    that = context.that,
			    el = context.el,
			    $deliveryId = el.$deliveryId,
			    $deliveryPrice = el.$deliveryPrice,
			    $inMkad = el.$inMkad,
			    $mkadDist = el.$mkadDist,
			    $address = el.$address;


			if (error === undefined) {
				$deliveryId.val(id);
				$deliveryPrice.val(price);
				$inMkad.val(inMKAD);
				$mkadDist.val(MkadDistance);

				el.data.deliveryIsValid = true;

				$address.trigger('field:error', { show: false });

				that.$window.trigger('cart:delivery', data);
			} else {
				el.data.deliveryIsValid = false;
				$address.trigger('field:error', { show: true, text: error });
			}
		}
	}, {
		key: 'goodsAdd',
		value: function goodsAdd(e) {
			var _e$data7 = e.data,
			    el = _e$data7.el,
			    that = _e$data7.that,
			    item = $(e.target).parent();


			el.$itemsWrap.append(item);

			if (el.$offers.find('div').length == 0) {
				el.$offTitle.hide();
			}
		}
	}, {
		key: 'goodsChange',
		value: function goodsChange(e, data) {
			var _e$data8 = e.data,
			    that = _e$data8.that,
			    el = _e$data8.el;


			console.log(data);
		}
	}]);

	return CartPage;
}();
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var CatalogPage = function () {
  function CatalogPage() {
    _classCallCheck(this, CatalogPage);

    this.selectors = {
      el: ".js-catalog-page",
      grid: ".js-catolog-grid",
      loader: ".js-catalog-loader",
      seo: ".js-catalog-seo"
    };

    this.classes = {
      loader: ".catalog-page__loader_show"
    };

    this.data = {};

    this.el = $(this.selectors.el);
    this.$window = $(window);

    if (this.el.length) {
      this.elems = [];
      this.get(this.selectors, this.data).init();
    }
  }

  _createClass(CatalogPage, [{
    key: "get",
    value: function get(_ref, data) {
      var slider = _ref.slider,
          fat = _ref.fat,
          seo = _ref.seo,
          fields = _ref.fields,
          form = _ref.form,
          grid = _ref.grid,
          loader = _ref.loader;

      this.elems = this.el.toArray().map(function (el) {
        var $el = $(el);
        return {
          $el: $el,
          $slider: $el.find(slider),
          $fat: $el.find(fat),
          $grid: $el.find(grid),
          $loader: $el.find(loader),
          $seo: $el.find(seo),
          data: {}
        };
      });
      return this;
    }
  }, {
    key: "init",
    value: function init() {
      var _this = this;

      this.elems.forEach(function (el) {
        _this.$window.on("menu:filterChange", { that: _this, el: el }, _this.getProducts);
        el.$seo.find(".catalog-page__seo-collapsible").click(function () {
          el.$seo.find(".catalog-page__seo-details").toggle();
        });
      });
    }
  }, {
    key: "getProducts",
    value: function getProducts(e, formData) {
      var _e$data = e.data,
          that = _e$data.that,
          el = _e$data.el,
          callback = that.setProducts,
          context = { that: that, el: el };


      var data = "";

      history.pushState(null, null, window.location.origin + window.location.pathname + "?" + formData.form);

      if (formData.params !== "") {
        data = formData.form + "&" + formData.params;
      } else {
        data = formData.form;
      }

      el.$loader.addClass(that.classes.loader);

      that.$window.trigger("ajax:get", {
        url: formData.url,
        dataType: "HTML",
        data: data,
        callback: callback,
        context: context
      });
    }
  }, {
    key: "setProducts",
    value: function setProducts(data) {
      var _data$context = data.context,
          that = _data$context.that,
          el = _data$context.el;


      el.$grid.find("*").remove();
      el.$grid.append(data.data);
      el.$loader.removeClass(that.classes.loader);

      that.$window.trigger("product:reload");
    }
  }]);

  return CatalogPage;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _objectDestructuringEmpty(obj) { if (obj == null) throw new TypeError("Cannot destructure undefined"); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Category = function () {
	function Category() {
		_classCallCheck(this, Category);

		this.selectors = {
			el: '.js-category',
			btn: '.js-category-btn'
		};

		this.classes = {
			show: 'category_show'
		};

		this.data = {};
		this.el = $(this.selectors.el);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data).init();
		}
	}

	_createClass(Category, [{
		key: 'get',
		value: function get(_ref, _ref2) {
			var btn = _ref.btn;

			_objectDestructuringEmpty(_ref2);

			this.elems = this.el.toArray().map(function (el) {
				var $el = $(el);
				return {
					$el: $el,
					$btn: $el.find(btn),
					data: {}
				};
			});
			return this;
		}
	}, {
		key: 'init',
		value: function init() {
			var _this = this;

			this.elems.forEach(function (el) {
				el.$btn.on('click', { that: _this, el: el }, _this.showMore);
			});
		}
	}, {
		key: 'showMore',
		value: function showMore(e) {
			var _e$data = e.data,
			    that = _e$data.that,
			    el = _e$data.el,
			    show = that.classes.show;


			el.$el.addClass(show);
		}
	}]);

	return Category;
}();
"use strict";
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ChangePass = function () {
	function ChangePass() {
		_classCallCheck(this, ChangePass);

		this.selectors = {
			el: '.js-change-pass',
			form: '.js-change-pass-form',
			field: '.js-change-pass-field',
			btn: '.js-change-pass-btn'
		};

		this.classes = {};

		this.el = $(this.selectors.el);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors).init();
		}
	}

	_createClass(ChangePass, [{
		key: 'get',
		value: function get(_ref) {
			var form = _ref.form,
			    field = _ref.field,
			    btn = _ref.btn;

			this.elems = this.el.toArray().map(function (el) {
				var $el = $(el);
				return {
					$el: $el,
					$form: $el.find(form),
					$field: $el.find(field),
					$btn: $el.find(btn)
				};
			});
			return this;
		}
	}, {
		key: 'init',
		value: function init() {
			var _this = this;

			this.elems.forEach(function (el) {
				el.$btn.on('click', { that: _this, el: el }, _this.submit);
			});
		}
	}, {
		key: 'submit',
		value: function submit(e) {
			e.preventDefault();

			var _e$data = e.data,
			    that = _e$data.that,
			    el = _e$data.el,
			    pass = [];

			var valid = true;

			$(el.$field[1]).trigger('field:error', { show: false });

			el.$field.each(function (i, field) {
				$(field).trigger('field:validate');
				if ($(field).data('error') === true) {
					valid = false;
				}
				pass[i] = $(field).find('.js-field-input').val();
			});

			if (valid && pass[0] !== pass[1]) {
				valid = false;
				$(el.$field[1]).trigger('field:error', { show: true, text: 'Пароли должны совпадать' });
			}

			if (valid) {
				el.$form.submit();
			}
		}
	}]);

	return ChangePass;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Comment = function () {
	function Comment() {
		_classCallCheck(this, Comment);

		this.selectors = {
			el: '.js-comment',
			add: '.js-comment-add',
			wrap: '.js-comment-wrap',
			field: '.js-field'
		};

		this.classes = {
			active: 'comment_active'
		};

		this.data = {};
		this.el = $(this.selectors.el);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors).init();
		}
	}

	_createClass(Comment, [{
		key: 'get',
		value: function get(_ref) {
			var add = _ref.add,
			    wrap = _ref.wrap,
			    field = _ref.field;

			this.elems = this.el.toArray().map(function (el) {
				var $el = $(el);
				return {
					$el: $el,
					$add: $el.find(add),
					$wrap: $el.find(wrap),
					$field: $el.find(field)
				};
			});
			return this;
		}
	}, {
		key: 'init',
		value: function init() {
			var _this = this;

			this.elems.forEach(function (el) {
				el.$add.on('click', { that: _this, el: el }, _this.toggleWrap);
				el.$field.on('field:change', _this.change);
			});
		}
	}, {
		key: 'toggleWrap',
		value: function toggleWrap(e) {
			e.preventDefault();

			var _e$data = e.data,
			    that = _e$data.that,
			    el = _e$data.el,
			    active = that.classes.active;


			el.$el.toggleClass(active);
			el.$field.trigger('field:empty');
		}
	}, {
		key: 'change',
		value: function change(e) {
			console.log('change');
		}
	}]);

	return Comment;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Counter = function () {
	function Counter() {
		_classCallCheck(this, Counter);

		this.selectors = {
			wrap: '.js-counter',
			native: '.js-counter-native',
			field: '.js-counter-field',
			minus: '.js-counter-minus',
			plus: '.js-counter-plus'
		};

		this.classes = {
			disabled: 'counter__btn_disabled'
		};

		this.data = {
			increment: 'increment',
			postfix: 'postfix',
			max: 'max',
			min: 'min',
			ratio: 'ratio',
			negative: 'negative',
			eventChange: 'event-change'
		};

		this.el = $(this.selectors.wrap);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data).init();
		}
	}

	_createClass(Counter, [{
		key: 'get',
		value: function get(_ref, data) {
			var _this = this;

			var native = _ref.native,
			    field = _ref.field,
			    minus = _ref.minus,
			    plus = _ref.plus;
			var increment = data.increment,
			    initial = data.initial,
			    postfix = data.postfix,
			    max = data.max,
			    min = data.min,
			    ratio = data.ratio,
			    negative = data.negative,
			    eventChange = data.eventChange;


			this.elems = this.el.toArray().map(function (el) {

				var $el = $(el),
				    minValue = $el.data(negative) ? Number.NEGATIVE_INFINITY : 0;

				return {
					$el: $el,
					$native: $el.find(native),
					$field: $el.find(field),
					$minus: $el.find(minus),
					$plus: $el.find(plus),
					data: {
						increment: _this.toFloat($el.data(increment)) || 1,
						value: _this.toFloat($el.find(native).val()),
						postfix: $el.data(postfix),
						max: _this.toInt($el.data(max)) || Number.POSITIVE_INFINITY,
						min: _this.toFloat($el.data(min)) || minValue,
						ratio: _this.toFloat($el.data(ratio)) || 1,
						eventChange: $el.data(eventChange) || 'counter:change'
					}
				};
			});
			return this;
		}
	}, {
		key: 'init',
		value: function init() {
			var _this2 = this;

			this.elems.forEach(function (elem) {
				elem.$minus.on('click', { that: _this2, elem: elem, add: false }, _this2.change);
				elem.$plus.on('click', { that: _this2, elem: elem, add: true }, _this2.change);
				elem.$el.on('counter:get', { that: _this2, elem: elem }, function (e) {
					return e.data.that.setValue(e.data.that, e.data.elem);
				});
			});
		}
	}, {
		key: 'change',
		value: function change(e) {
			var _e$data = e.data,
			    elem = _e$data.elem,
			    that = _e$data.that,
			    add = _e$data.add,
			    _elem$data = elem.data,
			    increment = _elem$data.increment,
			    negative = _elem$data.negative,
			    max = _elem$data.max,
			    min = _elem$data.min,
			    disabled = that.classes.disabled;

			var _that$getPossibility = that.getPossibility(elem.data),
			    mayIncrease = _that$getPossibility.mayIncrease,
			    mayDecrease = _that$getPossibility.mayDecrease;

			if (add && mayIncrease) {
				elem.data.value = that.add(elem.data.value, increment);
			}

			if (!add && mayDecrease) {
				elem.data.value = that.sub(elem.data.value, increment);
			}

			var _that$getPossibility2 = that.getPossibility(elem.data);

			mayIncrease = _that$getPossibility2.mayIncrease;
			mayDecrease = _that$getPossibility2.mayDecrease;

			elem.$plus.toggleClass(disabled, !mayIncrease);
			elem.$minus.toggleClass(disabled, !mayDecrease);

			that.setValue(that, elem);
		}
	}, {
		key: 'getPossibility',
		value: function getPossibility(_ref2) {
			var increment = _ref2.increment,
			    value = _ref2.value,
			    max = _ref2.max,
			    min = _ref2.min;

			var rate = 1000,
			    fixedVal = value * rate,
			    fixedInc = increment * rate;

			return {
				mayIncrease: fixedVal + fixedInc <= max * rate,
				mayDecrease: fixedVal - fixedInc >= min * rate
			};
		}
	}, {
		key: 'setValue',
		value: function setValue(that, elem) {
			var _elem$data2 = elem.data,
			    postfix = _elem$data2.postfix,
			    ratio = _elem$data2.ratio,
			    value = _elem$data2.value,
			    eventChange = _elem$data2.eventChange;

			var result = void 0,
			    resultPostfix = void 0,
			    resultComma = void 0;

			result = value * ratio;

			resultComma = result.toString().replace('.', ',');

			resultPostfix = resultComma + (postfix ? ' ' + postfix : '');

			elem.$native.val(value);
			elem.$field.text(resultPostfix);

			elem.$el.trigger(eventChange, { value: value, result: result });
		}
	}, {
		key: 'add',
		value: function add(a, b) {
			var rate = 1000;
			var res = a * rate + b * rate;
			return res / rate;
		}
	}, {
		key: 'sub',
		value: function sub(a, b) {
			var rate = 1000;
			var res = a * rate - b * rate;
			return res / rate;
		}
	}, {
		key: 'toInt',
		value: function toInt(data) {
			if (typeof data !== 'undefined' && data !== null) {
				return parseInt(data);
			} else {
				return undefined;
			}
		}
	}, {
		key: 'toFloat',
		value: function toFloat(data) {
			if (typeof data !== 'undefined' && data !== null) {
				return parseFloat(data);
			} else {
				return undefined;
			}
		}
	}]);

	return Counter;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Field = function () {
	function Field() {
		_classCallCheck(this, Field);

		this.selectors = {
			el: '.js-field',
			label: '.js-field-label',
			input: '.js-field-input',
			clear: '.js-field-clear',
			extra: '.js-field-extra',
			wrap: '.js-field-wrap',
			error: '.js-field-error'
		};

		this.data = {
			mask: 'mask',
			clearEvent: 'clear-event',
			eventFocus: 'event-focus',
			eventError: 'event-error',
			eventFocusOut: 'event-focus-out'
		};

		this.classes = {
			focus: 'field_focus',
			error: 'field_error',
			extra: 'field_extra',
			checkbox: 'field_checkbox',
			checked: 'field_checked'
		};

		this.el = $(this.selectors.el);
		this.$window = $(window);

		if (this.el.length) {
			this.elems = [];
			this.get().init();
		}
	}

	_createClass(Field, [{
		key: 'get',
		value: function get() {
			this.elems = this.el.toArray().map(this.addEl, this);
			return this;
		}
	}, {
		key: 'addEl',
		value: function addEl(el) {
			var _selectors = this.selectors,
			    input = _selectors.input,
			    clear = _selectors.clear,
			    extra = _selectors.extra,
			    wrap = _selectors.wrap,
			    error = _selectors.error,
			    label = _selectors.label,
			    _data = this.data,
			    clearEvent = _data.clearEvent,
			    eventFocus = _data.eventFocus,
			    eventError = _data.eventError,
			    mask = _data.mask,
			    eventFocusOut = _data.eventFocusOut,
			    $el = $(el);


			return {
				$el: $el,
				$input: $el.find(input),
				$label: $el.find(label),
				$clear: $el.find(clear),
				$extra: $el.find(extra),
				$wrap: $el.find(wrap),
				$error: $el.find(error),
				data: {
					mask: $el.data(mask),
					clear: $el.data(clearEvent) || 'field:clear',
					focus: $el.data(eventFocus) || 'field:focus',
					error: $el.data(eventError) || 'field:error',
					focusOut: $el.data(eventFocusOut) || 'field:focusout',
					checkbox: $el.hasClass(this.classes.checkbox)
				}
			};
		}
	}, {
		key: 'init',
		value: function init() {
			this.elems.forEach(this.initEl, this);
			this.$window.on('field:add', { that: this }, this.newEl);
		}
	}, {
		key: 'initEl',
		value: function initEl(elem) {
			elem.$input.on('focusin focusout', { elem: elem, that: this }, this.focus).on('change', { elem: elem, that: this }, this.change).on('input', { elem: elem }, this.input);

			elem.$el.on('field:update', { elem: elem, that: this }, this.updateValue).on('field:extra', { elem: elem, that: this }, this.extra).on('field:empty', { elem: elem }, this.empty).on('field:validate', { elem: elem, that: this }, this.runValidate).on('field:disable', { elem: elem, that: this }, this.setDisabled).on(elem.data.focus, { elem: elem }, this.setFocus).on(elem.data.error, { elem: elem, that: this }, this.showError).on('field:copy', { elem: elem, that: this }, this.copyAction);

			if (elem.data.checkbox) {
				elem.$wrap.on('click', { elem: elem, that: this }, this.checkboxChange);
			}

			if (elem.data.mask) {
				elem.numbered = new Numbered(elem.$input, {
					mask: elem.data.mask,
					empty: '_'
				});
			}

			elem.$clear.on('click', { elem: elem }, this.clear);
		}
	}, {
		key: 'copyAction',
		value: function copyAction(e) {
			var _e$data = e.data,
			    elem = _e$data.elem,
			    that = _e$data.that;


			elem.$input[0].select();
			document.execCommand('copy');
			elem.$el.trigger('field:copied');
		}
	}, {
		key: 'setDisabled',
		value: function setDisabled(e, _ref) {
			var disabled = _ref.disabled;
			var _e$data2 = e.data,
			    elem = _e$data2.elem,
			    that = _e$data2.that;


			elem.$input.prop('readonly', disabled);
		}
	}, {
		key: 'newEl',
		value: function newEl(e, data) {
			var that = e.data.that,
			    el = data.el;


			var newEl = that.addEl(el[0]);
			that.elems.push(newEl);
			that.initEl(newEl);
		}
	}, {
		key: 'showError',
		value: function showError(e, data) {
			var _e$data3 = e.data,
			    elem = _e$data3.elem,
			    that = _e$data3.that,
			    _that$classes = that.classes,
			    extra = _that$classes.extra,
			    error = _that$classes.error,
			    show = data.show,
			    text = data.text;


			elem.$el.removeClass(extra);

			if (text !== undefined) {
				elem.$error.text(text);
			}
			elem.$el.toggleClass(error, show);
		}
	}, {
		key: 'extra',
		value: function extra(e) {
			var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
			var _e$data4 = e.data,
			    that = _e$data4.that,
			    elem = _e$data4.elem,
			    text = data.text,
			    _that$classes2 = that.classes,
			    extra = _that$classes2.extra,
			    error = _that$classes2.error,
			    show = data.show !== false;


			elem.$el.removeClass(error);

			if (text !== undefined) {
				elem.$extra.text(text);
			}
			elem.$el.toggleClass(extra, show);
		}
	}, {
		key: 'setFocus',
		value: function setFocus(e) {
			var elem = e.data.elem;


			elem.$input.focus();
		}
	}, {
		key: 'clear',
		value: function clear(e) {
			var elem = e.data.elem,
			    $input = elem.$input,
			    $label = elem.$label,
			    value = $input.val(),
			    label = $label ? $label.text() : false;


			elem.$el.trigger(elem.data.clear, { value: value, label: label });
		}
	}, {
		key: 'empty',
		value: function empty(e) {
			var $input = e.data.elem.$input;


			$input.val('');
		}
	}, {
		key: 'checkboxChange',
		value: function checkboxChange(e) {
			var _e$data5 = e.data,
			    that = _e$data5.that,
			    elem = _e$data5.elem,
			    checked = that.classes.checked,
			    isChecked = elem.$el.hasClass(checked);


			elem.$input.prop('checked', !isChecked).trigger('change');
		}
	}, {
		key: 'change',
		value: function change(e) {
			var _e$data6 = e.data,
			    that = _e$data6.that,
			    elem = _e$data6.elem,
			    _e$data$elem = e.data.elem,
			    $el = _e$data$elem.$el,
			    $input = _e$data$elem.$input,
			    numbered = _e$data$elem.numbered,
			    type = $el.data().validate,
			    checked = that.classes.checked,
			    $label = elem.$label;

			var value = $input.val();
			var label = $label ? $label.text() : false;

			$el.trigger('field:change', { value: value, label: label });

			if (elem.data.checkbox) {
				$el.toggleClass(checked, $input.prop('checked'));
				value = $input.prop('checked');
			}

			if (type) {
				that.validate({ that: that, $el: $el, type: type, value: value, numbered: numbered });
			}
		}
	}, {
		key: 'input',
		value: function input(e) {
			var elem = e.data.elem,
			    value = elem.$input.val();


			elem.$el.trigger('field:input', { value: value });
		}
	}, {
		key: 'runValidate',
		value: function runValidate(e) {
			var _e$data7 = e.data,
			    that = _e$data7.that,
			    elem = _e$data7.elem,
			    $el = elem.$el,
			    $input = elem.$input,
			    numbered = elem.numbered,
			    checked = $input.prop('checked'),
			    type = $el.data().validate;

			var value = $input.val();

			if (elem.data.checkbox) {
				value = $input.prop('checked');
			}

			if (type) {
				that.validate({ that: that, $el: $el, type: type, value: value, numbered: numbered });
			}
		}
	}, {
		key: 'validate',
		value: function validate(_ref2) {
			var that = _ref2.that,
			    $el = _ref2.$el,
			    type = _ref2.type,
			    value = _ref2.value,
			    numbered = _ref2.numbered;

			var text = /^(\w|\d|[А-я,Ё,ё]){2,}/;
			var notEmpty = /.{2,}/;
			var email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

			var hasError = false;

			if (value !== '') {
				switch (type) {
					case 'text':
						hasError = value.match(text) === null;
						break;
					case 'notEmpty':
						hasError = value.match(notEmpty) === null;
						break;
					case 'email':
						hasError = value.toLowerCase().match(email) === null;
						break;
					case 'tel':
						hasError = value.replace(/[^\d]/gi, '').length < 10;
						break;
					case 'checkbox':
						hasError = !value;
						break;
					case 'mask':
						hasError = numbered.validate() < 0;
						break;
				}
			} else {
				hasError = true;
			}

			$el.data('error', hasError);
			$el.toggleClass(that.classes.error, hasError);
		}
	}, {
		key: 'updateValue',
		value: function updateValue(e, data) {
			var _e$data8 = e.data,
			    elem = _e$data8.elem,
			    that = _e$data8.that,
			    value = data.value;


			elem.$input.val(value);
		}
	}, {
		key: 'focus',
		value: function focus(e) {
			var _e$data9 = e.data,
			    elem = _e$data9.elem,
			    that = _e$data9.that;

			elem.$el.toggleClass(that.classes.focus, e.type == 'focusin');
			if (e.type == 'focusout') {
				elem.$el.trigger(elem.data.focusOut);
			}
		}
	}]);

	return Field;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var FieldOptions = function () {
	function FieldOptions() {
		_classCallCheck(this, FieldOptions);

		this.selectors = {
			elem: '.js-field-options',
			option: '.js-option',
			field: '.js-field'
		};

		this.classes = {};

		this.data = {
			eventChange: 'event-change'
		};

		this.el = $(this.selectors.elem);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data).init();
		}
	}

	_createClass(FieldOptions, [{
		key: 'get',
		value: function get(_ref, _ref2) {
			var option = _ref.option,
			    field = _ref.field;
			var eventChange = _ref2.eventChange;

			this.elems = this.el.toArray().map(function (el) {
				var $el = $(el);

				return {
					$el: $el,
					$options: $el.find(option),
					$field: $el.find(field),
					data: {
						change: $el.data(eventChange) || 'field-option:change'
					}
				};
			});
			return this;
		}
	}, {
		key: 'init',
		value: function init() {
			var _this = this;

			this.elems.forEach(function (el) {
				el.$options.on('click', { el: el, that: _this }, _this.optionClick);
				el.$el.on('field-option:validate', { el: el, that: _this }, _this.validate);
			});
		}
	}, {
		key: 'validate',
		value: function validate(e) {
			var _e$data = e.data,
			    el = _e$data.el,
			    that = _e$data.that;


			el.$field.trigger('field:validate');
		}
	}, {
		key: 'optionClick',
		value: function optionClick(e) {
			var _e$data2 = e.data,
			    el = _e$data2.el,
			    that = _e$data2.that,
			    $cur = $(e.currentTarget),
			    value = $cur.text(),
			    data = $cur.data();


			el.$field.trigger('field:update', { value: value });
			// .trigger('field:focus');
			el.$el.trigger(el.data.change, { value: value, data: data });
		}
	}]);

	return FieldOptions;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Footer = function () {
	function Footer() {
		_classCallCheck(this, Footer);

		this.selectors = {
			footer: '.js-footer'
		};

		this.classes = {};

		this.el = $(this.selectors.footer);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors).init();
		}
	}

	_createClass(Footer, [{
		key: 'get',
		value: function get() {
			this.elems = this.el.toArray().map(function (el) {
				var $el = $(el);
				return {
					$el: $el
				};
			});
			return this;
		}
	}, {
		key: 'init',
		value: function init() {
			this.elems.forEach(function (elem) {
				// elem.$menuBtn.on('click', {that:this, elem}, this.mobileMenu);
			});
		}
	}, {
		key: 'mobileSearch',
		value: function mobileSearch(e) {
			var _e$data = e.data,
			    that = _e$data.that,
			    elem = _e$data.elem;
			var mobileSearch = that.classes.mobileSearch;


			elem.$el.addClass(mobileSearch);
		}
	}]);

	return Footer;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var FriendPage = function () {
	function FriendPage() {
		_classCallCheck(this, FriendPage);

		this.selectors = {
			el: '.js-friend-page',
			copy: '.js-friend-page-copy',
			copyBtn: '.js-friend-page-copy-btn',
			form: '.friend-page__form',
			invite: '.js-friend-page-invite',
			inviteBtn: '.js-friend-page-invite-btn',
			inviteInfo: '.js-friend-page-invite-info'
		};

		this.classes = {
			formInfo: 'friend-page__form_info'
		};

		this.data = {};

		this.el = $(this.selectors.el);
		this.$window = $(window);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data).init();
		}
	}

	_createClass(FriendPage, [{
		key: 'get',
		value: function get(_ref, data) {
			var copy = _ref.copy,
			    copyBtn = _ref.copyBtn,
			    invite = _ref.invite,
			    inviteBtn = _ref.inviteBtn,
			    inviteInfo = _ref.inviteInfo;
			var eventGoodsAdd = data.eventGoodsAdd;


			this.elems = this.el.toArray().map(function (el) {
				var $el = $(el);
				return {
					$el: $el,
					$copy: $el.find(copy),
					$copyBtn: $el.find(copyBtn),
					$invite: $el.find(invite),
					$inviteBtn: $el.find(inviteBtn),
					$inviteInfo: $el.find(inviteInfo),
					data: {}
				};
			});
			return this;
		}
	}, {
		key: 'init',
		value: function init() {
			var _this = this;

			this.elems.forEach(function (el) {
				el.$inviteBtn.on('click', { that: _this, el: el }, _this.invite);
				el.$copyBtn.on('click', { that: _this, el: el }, _this.copyClick);
				el.$copy.on('field:copied', { that: _this, el: el }, _this.showInfo);
			});
		}
	}, {
		key: 'invite',
		value: function invite(e) {
			e.preventDefault();

			var _e$data = e.data,
			    el = _e$data.el,
			    that = _e$data.that,
			    form = that.selectors.form,
			    $form = $(e.currentTarget).parents(form),
			    data = $form.serialize(),
			    url = $form.prop('action'),
			    callback = that.inviteSended;


			el.$invite.trigger('field:validate');
			if (el.$invite.data('error') === false) {
				that.$window.trigger('ajax:get', { url: url, data: data, callback: callback, context: { that: that, el: el, target: e.currentTarget } });
			}
		}
	}, {
		key: 'inviteSended',
		value: function inviteSended(e) {
			var _e$context = e.context,
			    el = _e$context.el,
			    that = _e$context.that,
			    target = _e$context.target,
			    message = e.data.message;


			el.$inviteInfo.text(message);
			that.showInfo({ data: { that: that, el: el }, currentTarget: target });
		}
	}, {
		key: 'copyClick',
		value: function copyClick(e) {
			var _e$data2 = e.data,
			    el = _e$data2.el,
			    that = _e$data2.that;


			el.$copy.trigger('field:copy');
		}
	}, {
		key: 'showInfo',
		value: function showInfo(e) {
			var _e$data3 = e.data,
			    el = _e$data3.el,
			    that = _e$data3.that,
			    form = that.selectors.form,
			    formInfo = that.classes.formInfo,
			    $form = $(e.currentTarget).parents(form);


			$form.addClass(formInfo);

			setTimeout(function () {
				that.hideInfo(el, $form);
			}.bind(that), 2000);
		}
	}, {
		key: 'hideInfo',
		value: function hideInfo(el, $form) {
			var formInfo = this.classes.formInfo;


			$form.removeClass(formInfo);
		}
	}]);

	return FriendPage;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Head = function () {
	function Head() {
		_classCallCheck(this, Head);

		this.selectors = {
			head: '.js-head',
			menuBtn: '.js-head-menu-btn',
			shade: '.js-head-shade',
			search: '.js-mobile-search',
			searchForm: '.js-head-search-form',
			searchList: '.js-head-search-list',
			searchAdd: '.js-head-search-add',
			searchBtn: '.js-head-search-btn',
			login: '.js-head-login',
			loginLink: '.js-head-login-link',
			loginTurn: '.js-head-login-turn',
			loginBtn: '.js-head-login-btn',
			loginMsg: '.js-head-login-msg',
			cart: {
				cart: '.js-cart',
				btn: '.js-cart-btn',
				amount: '.js-cart-amount',
				list: '.js-cart-list',
				order: '.js-cart-order'
			}
		};

		this.classes = {
			hideTop: 'head_top-hide',
			mobileMenu: 'head_mobile-menu',
			expandSearch: 'head_expand-search',
			cartListOpen: 'head_cart-list-open',
			search: 'head_search',
			login: 'head_login',
			recovery: 'head__login_recovery',
			amountHide: 'head__cart-amount_hide'
		};

		this.data = {
			cartAddUrl: 'cart-add-url',
			eventAdd: 'event-add'
		};

		this.desktopWidth = 1000;

		this.$window = $(window);
		this.el = $(this.selectors.head);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data).init();
		}
	}

	_createClass(Head, [{
		key: 'get',
		value: function get(_ref, _ref2) {
			var menuBtn = _ref.menuBtn,
			    shade = _ref.shade,
			    search = _ref.search,
			    searchForm = _ref.searchForm,
			    cart = _ref.cart,
			    searchList = _ref.searchList,
			    login = _ref.login,
			    loginTurn = _ref.loginTurn,
			    loginLink = _ref.loginLink,
			    loginBtn = _ref.loginBtn,
			    loginMsg = _ref.loginMsg;
			var cartAddUrl = _ref2.cartAddUrl,
			    eventAdd = _ref2.eventAdd;

			this.elems = this.el.toArray().map(function (el) {
				var $el = $(el);
				return {
					$el: $el,
					$menuBtn: $el.find(menuBtn),
					$shade: $el.find(shade),
					$search: $el.find(search),
					$searchForm: $el.find(searchForm),
					$searchList: $el.find(searchList),
					$searchField: $el.find(search).parent().find('.js-field'),
					login: {
						$login: $el.find(login),
						$loginLink: $el.find(loginLink),
						$loginTurn: $el.find(loginTurn),
						$loginBtns: $el.find(loginBtn),
						$loginFields: $el.find(login + ' .js-field'),
						$loginMsg: $el.find(loginMsg)
					},
					cart: {
						$cart: $el.find(cart.cart),
						$btn: $el.find(cart.btn),
						$amount: $el.find(cart.amount),
						$list: $el.find(cart.list),
						$order: $el.find(cart.order)
					},
					data: {
						cartAddUrl: $el.data(cartAddUrl),
						eventAdd: $el.data(eventAdd) || 'cart:add',
						showList: true,
						searchItemCount: $el.find(searchForm).data('count')
					}
				};
			});
			return this;
		}
	}, {
		key: 'init',
		value: function init() {
			var _this = this;

			this.checkMobile();
			this.$window.on('resize', { that: this }, this.checkMobile);

			this.elems.forEach(function (elem) {
				Stickyfill.add(elem.$el);

				elem.$menuBtn.on('click', { that: _this, elem: elem }, _this.mobileMenu);
				elem.$shade.on('click', { that: _this, elem: elem }, _this.hideShade);
				elem.$search.on('click', { that: _this, elem: elem }, _this.expandSearch);
				elem.$searchField.on('focusin', { that: _this, elem: elem }, _this.expandSearch).on('field:input', { that: _this, elem: elem }, _this.searchSet);

				elem.login.$loginLink.on('click', { that: _this, elem: elem }, _this.toggleLogin);
				elem.login.$loginTurn.on('click', { that: _this, elem: elem }, _this.loginTurn);
				elem.login.$loginFields.on('field:input', { that: _this, elem: elem }, _this.validateLoginForm);
				elem.login.$loginBtns.on('click', { that: _this, elem: elem }, _this.sendLoginForm);

				// elem.cart.$btn.on('click', {that:this, elem}, this.toggleCartList);
				// elem.cart.$amount.on('click', {that:this, elem}, this.toggleCartList);

				_this.$window
				// .on('scroll', {that: this, elem}, this.hideTop)
				.on('cart:update cart:add', { that: _this, elem: elem }, _this.renderCart).on('cart:dontShowList', { elem: elem }, function (e) {
					return e.data.elem.data.showList = false;
				});
			});
		}
	}, {
		key: 'loginTurn',
		value: function loginTurn(e) {
			var _e$data = e.data,
			    that = _e$data.that,
			    el = _e$data.elem,
			    recovery = that.classes.recovery;


			el.login.$login.toggleClass(recovery);
		}
	}, {
		key: 'toggleLogin',
		value: function toggleLogin(e) {
			var _e$data2 = e.data,
			    that = _e$data2.that,
			    elem = _e$data2.elem,
			    login = that.classes.login;


			elem.$el.toggleClass(login);
		}
	}, {
		key: 'toggleCartList',
		value: function toggleCartList(e) {
			var _e$data3 = e.data,
			    that = _e$data3.that,
			    elem = _e$data3.elem,
			    cartListOpen = that.classes.cartListOpen;


			elem.$el.toggleClass(cartListOpen);
		}
	}, {
		key: 'checkMobile',
		value: function checkMobile(e) {
			var that = {};

			if (e !== undefined && e !== null) {
				that = e.data.that;
			} else {
				that = this;
			}

			that.isMobile = window.innerWidth < this.desktopWidth;
		}

		// hideTop(e) {
		// 	const {that, elem} = e.data;

		// 	if(!that.isMobile){
		// 		const needHide = window.scrollY > 10;
		// 		const {hideTop} = that.classes;

		// 		elem.$el.toggleClass(hideTop, needHide);
		// 	}
		// }

	}, {
		key: 'mobileMenu',
		value: function mobileMenu(e) {
			var _e$data4 = e.data,
			    that = _e$data4.that,
			    elem = _e$data4.elem;
			var mobileMenu = that.classes.mobileMenu;


			elem.$el.toggleClass(mobileMenu);
		}
	}, {
		key: 'hideShade',
		value: function hideShade(e) {
			var _e$data5 = e.data,
			    that = _e$data5.that,
			    elem = _e$data5.elem;
			var _that$classes = that.classes,
			    mobileMenu = _that$classes.mobileMenu,
			    search = _that$classes.search,
			    login = _that$classes.login;


			if (elem.$el.hasClass(mobileMenu)) {
				that.mobileMenu({ data: e.data });
			}

			elem.$el.toggleClass(login, false);

			if (elem.$el.hasClass(search)) {
				elem.$el.removeClass(search);
				elem.$searchList.fadeOut();
			};
		}
	}, {
		key: 'expandSearch',
		value: function expandSearch(e) {
			var _e$data6 = e.data,
			    that = _e$data6.that,
			    elem = _e$data6.elem,
			    expandSearch = that.classes.expandSearch;


			elem.$el.addClass(expandSearch);

			if (e.type != 'focusin') {
				elem.$searchField.trigger('field:focus');
			};

			elem.$searchField.on('field:focusout', { that: that, elem: elem }, that.turnSearch);
		}
	}, {
		key: 'turnSearch',
		value: function turnSearch(e) {
			var _e$data7 = e.data,
			    that = _e$data7.that,
			    elem = _e$data7.elem,
			    _that$classes2 = that.classes,
			    expandSearch = _that$classes2.expandSearch,
			    search = _that$classes2.search;


			elem.$el.removeClass(expandSearch);
			elem.$searchField.off('field:focusOut');
		}
	}, {
		key: 'renderCart',
		value: function renderCart(e, _ref3) {
			var count = _ref3.cart_count,
			    sum = _ref3.cart_sum,
			    items = _ref3.Items;
			var _e$data8 = e.data,
			    that = _e$data8.that,
			    el = _e$data8.elem,
			    _that$classes3 = that.classes,
			    cartListOpen = _that$classes3.cartListOpen,
			    amountHide = _that$classes3.amountHide;

			var list = '';

			if (count > 0) {
				el.cart.$amount.text(count).removeClass(amountHide);
			}

			for (var _el in items) {
				var _items$_el = items[_el],
				    name = _items$_el.Name,
				    price = _items$_el.TotalPrice;

				list += '\n\t\t\t<div class="head__cart-list-el">\n\t\t\t\t<div class="head__cart-list-el-title">' + name + '</div>\n\t\t\t\t<div class="head__cart-list-el-price">' + price + ' \u20BD</div>\n\t\t\t\t</div>\n\t\t\t';
			};

			el.cart.$list.text('').html(list);
			el.cart.$order.text('\u0417\u0430\u043A\u0430\u0437\u0430\u0442\u044C \u0437\u0430 ' + sum + ' \u20BD');

			if (el.data.showList) {
				el.$el.addClass(cartListOpen);
				that.$window.on('click', { that: that, el: el }, that.closeCartList);
			}
		}
	}, {
		key: 'closeCartList',
		value: function closeCartList(e) {
			var _e$data9 = e.data,
			    that = _e$data9.that,
			    el = _e$data9.el,
			    cartListOpen = that.classes.cartListOpen,
			    clicked = $(e.currentTarget);


			if (!clicked.hasClass(that.selectors.cart.order)) {
				el.$el.removeClass(cartListOpen);
			}

			that.$window.off('click', that.closeCartList);
		}
	}, {
		key: 'searchSet',
		value: function searchSet(e, data) {
			var _e$data10 = e.data,
			    that = _e$data10.that,
			    el = _e$data10.elem,
			    value = data.value;


			if (el.data.timer) {
				clearTimeout(el.data.timer);
			}
			if (value != '') {
				el.data.timer = setTimeout(that.sendQuery, 1000, { that: that, el: el });
			} else {
				that.hideShade({ data: { that: that, elem: el } });
			}
		}
	}, {
		key: 'sendQuery',
		value: function sendQuery(data) {
			var that = data.that,
			    el = data.el,
			    url = el.$searchForm.data('action'),
			    callback = that.renderSearch,
			    context = { that: that, el: el };

			var formData = el.$searchForm.serialize();

			formData += '&' + $.param(el.$searchForm.data());

			that.$window.trigger('ajax:get', { url: url, data: formData, callback: callback, context: context });
		}
	}, {
		key: 'renderSearch',
		value: function renderSearch(_ref4) {
			var data = _ref4.data,
			    context = _ref4.context;
			var that = context.that,
			    el = context.el,
			    _that$selectors = that.selectors,
			    searchAdd = _that$selectors.searchAdd,
			    searchBtn = _that$selectors.searchBtn;

			var list = '';

			for (var key in data.Items) {
				var _el2 = data.Items[key];
				list += '\n\t\t\t<a href="' + _el2.URL + '" class="head__search-list-el">\n\t\t\t\t<span class="head__search-list-img-wrap"><img class="head__search-list-img" src="' + _el2.Image + '" alt="' + _el2.FullName + '">\n\t\t\t\t</span>\n\t\t\t\t<span class="head__search-list-params">\n\t\t\t\t\t<span class="head__search-list-price">' + _el2.ItemPrice + ' \u20BD</span>\n\t\t\t\t\t<span class="head__search-list-weight">' + _el2.Units + '</span>\n\t\t\t\t</span>\n\t\t\t\t<span class="head__search-list-subtitle">' + (_el2.NamePrefix + ' ' + _el2.Name) + '</span>\n\t\t\t\t<span class="head__search-list-add">\n\t\t\t\t\t<span class="head__search-list-title">' + _el2.Name + '</span>\n\t\t\t\t\t<span class="head__search-list-icon js-head-search-add" data-' + _el2.Field + '=' + _el2.Count + ' data-json="1" data-cart_mode="add">\n\t\t\t\t\t\t<svg class="icon">\n\t\t\t\t\t\t\t<use xlink:href="#cart"></use>\n\t\t\t\t\t\t</svg>\n\t\t\t\t\t</span>\n\t\t\t\t</span>\n\t\t\t</a>';
			};
			if (data.total > el.data.searchItemCount) {
				list += '<button class="button js-button head__search-list-button js-head-search-btn">\u041F\u043E\u043A\u0430\u0437\u0430\u0442\u044C \u0432\u0441\u0435 ' + data.total + '</button>';
			}
			el.$searchList.html(list);
			el.$el.addClass(that.classes.search).find(searchAdd).on('click', { that: that, el: el }, that.addToCart);

			el.$searchList.fadeIn();

			el.$el.find(searchBtn).on('click', { that: that, el: el }, that.goToCart);
		}
	}, {
		key: 'goToCart',
		value: function goToCart(e) {
			e.preventDefault();

			var _e$data11 = e.data,
			    el = _e$data11.el,
			    that = _e$data11.that;


			el.$searchForm.submit();
		}
	}, {
		key: 'addToCart',
		value: function addToCart(e) {
			e.preventDefault();

			var _e$data12 = e.data,
			    that = _e$data12.that,
			    el = _e$data12.el,
			    $target = $(e.currentTarget),
			    url = el.data.cartAddUrl,
			    data = $target.data(),
			    callback = that.addResponce,
			    context = { that: that, el: el };


			that.$window.trigger('ajax:get', { url: url, data: $.param(data), callback: callback, context: context });
		}
	}, {
		key: 'addResponce',
		value: function addResponce(data) {
			var _data$context = data.context,
			    that = _data$context.that,
			    el = _data$context.el;


			that.$window.trigger(el.data.eventAdd, data.data);
		}
	}, {
		key: 'validateLoginForm',
		value: function validateLoginForm(e) {
			e.preventDefault();

			var _e$data13 = e.data,
			    that = _e$data13.that,
			    el = _e$data13.el,
			    $form = $(e.currentTarget).parents('form');

			var valid = true;

			$form.find('.js-field').each(function (i, item) {
				var $el = $(item);
				$el.trigger('field:validate');
				if ($el.data('error') === true) {
					valid = false;
				}
			});

			$form.find('.js-button').prop('disabled', !valid);
		}
	}, {
		key: 'sendLoginForm',
		value: function sendLoginForm(e) {
			e.preventDefault();

			var _e$data14 = e.data,
			    that = _e$data14.that,
			    el = _e$data14.elem,
			    $form = $(e.target).parents('form'),
			    url = $form.prop('action'),
			    data = $form.serialize(),
			    callback = that.loginResponce,
			    context = { that: that, el: el, recovery: $form.hasClass('head__login-recovery') };


			that.$window.trigger('ajax:get', { url: url, data: data, callback: callback, context: context });
		}
	}, {
		key: 'loginResponce',
		value: function loginResponce(data) {
			var _data$context2 = data.context,
			    el = _data$context2.el,
			    that = _data$context2.that,
			    recovery = _data$context2.recovery,
			    _data$data = data.data,
			    result = _data$data.result,
			    message = _data$data.message,
			    $loginMsg = el.login.$loginMsg;


			if (recovery) {
				$loginMsg.text(message);
			} else {
				if (result) {
					location.reload();
				} else {
					$loginMsg.text(message);
				}
			}
		}
	}]);

	return Head;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Menu = function () {
	function Menu() {
		_classCallCheck(this, Menu);

		this.selectors = {
			el: '.js-menu',
			slider: '.js-slider',
			fat: '.js-menu-fat-val',
			form: '.js-menu-form',
			fields: '.js-field',

			sub: '.js-menu-sub',
			subItem: '.js-menu-sub-item',
			subHidden: '.js-menu-sub-hidden',
			subHiddenWrap: '.js-menu-sub-hidden-wrap',

			filterSlider: '.js-menu-filter-slider',
			filterItem: '.js-menu-filter-item',
			filterHidden: '.js-menu-filter-hidden',
			filterHiddenWrap: '.js-menu-filter-hidden-wrap',

			topMore: '.js-menu-top-more'
		};

		this.classes = {
			subTrim: 'menu__sub_trim',
			subItemHide: 'menu__sub-item_hide',

			filterTrim: 'menu__filter_trim',
			filterItemHide: 'menu__filter-check-el_hide',

			short: 'menu_short'
		};

		this.data = {};

		this.el = $(this.selectors.el);
		this.$window = $(window);

		this.desktopWidth = 1000;

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors).init();
		}
	}

	_createClass(Menu, [{
		key: 'get',
		value: function get(sel) {
			this.elems = this.el.toArray().map(function (el) {
				var $el = $(el);
				return {
					$el: $el,
					$slider: $el.find(sel.slider),
					$fat: $el.find(sel.fat),
					$form: $el.find(sel.form),
					$fields: $el.find(sel.fields),

					$sub: $el.find(sel.sub),
					$subItems: $el.find(sel.subItem),
					$subHidden: $el.find(sel.subHidden),
					$subHiddenWrap: $el.find(sel.subHiddenWrap),

					$filterItems: $el.find(sel.filterItem),
					$filterSlider: $el.find(sel.filterSlider),
					$filterHidden: $el.find(sel.filterHidden),
					$filterHiddenWrap: $el.find(sel.filterHiddenWrap),

					$topMore: $el.find(sel.topMore),

					data: {
						subParams: {
							items: [],
							hiddenWrap: 0
						},
						filterParams: {
							items: [],
							hiddenWrap: 0,
							slider: 0
						}
					}
				};
			});
			return this;
		}
	}, {
		key: 'init',
		value: function init() {
			var _this = this;

			this.elems.forEach(function (el) {
				el.$slider.on('slider:update', { that: _this, el: el }, _this.changeFat).on('slider:change', { that: _this, el: el }, _this.change);
				el.$fields.on('field:change', { that: _this, el: el }, _this.change);

				el.$topMore.on('click', { that: _this, el: el }, _this.showTopItems);

				_this.$window.on('resize', { that: _this, el: el }, _this.runOverflowCheck);

				_this.getOverflowElsParams(el);
				_this.runOverflowCheck({ data: { el: el, that: _this } });
				// Stickyfill.add(el.$el);
			});
		}
	}, {
		key: 'showTopItems',
		value: function showTopItems(e) {
			var _e$data = e.data,
			    that = _e$data.that,
			    el = _e$data.el,
			    short = that.classes.short;


			el.$el.removeClass(short);
		}
	}, {
		key: 'getOverflowElsParams',
		value: function getOverflowElsParams(el) {
			el.$subItems.each(function (i, item) {
				el.data.subParams.items.push(item.offsetWidth);
			});
			el.data.subParams.hiddenWrap = el.$subHidden.innerWidth() + 50;

			el.$filterItems.each(function (i, item) {
				el.data.filterParams.items.push($(item).innerWidth());
			});
			el.data.filterParams.hiddenWrap = el.$filterHidden.innerWidth() + 60;
			el.data.filterParams.slider = el.$filterSlider.innerWidth();
		}
	}, {
		key: 'runOverflowCheck',
		value: function runOverflowCheck(e) {
			var _e$data2 = e.data,
			    that = _e$data2.that,
			    el = _e$data2.el,
			    classes = that.classes;


			if (!window.innerWidth < that.desktopWidth) {
				that.checkOverflow(el, el.data.subParams, el.$subItems, el.$sub, el.$subHiddenWrap, { trim: classes.subTrim, itemHide: classes.subItemHide });

				that.checkOverflow(el, el.data.filterParams, el.$filterItems, el.$form, el.$filterHiddenWrap, { trim: classes.filterTrim, itemHide: classes.filterItemHide });
			}
		}
	}, {
		key: 'checkOverflow',
		value: function checkOverflow(el, params, items, wrap, hiddenWrap, _ref) {
			var _this2 = this;

			var trim = _ref.trim,
			    itemHide = _ref.itemHide;

			var wrapWidth = wrap.innerWidth(),
			    slider = params.slider ? params.slider : 0;

			var totalWidth = 0,
			    overflow = false;

			hiddenWrap.children().remove();
			items.removeClass(itemHide);

			items.each(function (i, item) {
				totalWidth += params.items[i];

				if (totalWidth + params.hiddenWrap + slider >= wrapWidth) {
					overflow = true;
					if (slider) {
						var clone = $(item).clone(true);
						clone.find('.js-field-input').prop('id', 'temp' + i);
						clone.find('.js-field-label').prop('for', 'temp' + i);
						clone.appendTo(hiddenWrap);
					} else {
						$(item).clone().appendTo(hiddenWrap);
					}
					$(item).addClass(itemHide);
				}
			});

			if (slider) {
				hiddenWrap.find('.js-field').each(function (i, item) {
					_this2.$window.trigger('field:add', { el: $(item) });
				});
			}

			wrap.toggleClass(trim, overflow);
		}
	}, {
		key: 'changeFat',
		value: function changeFat(e, data) {
			var _e$data3 = e.data,
			    that = _e$data3.that,
			    el = _e$data3.el,
			    start = data.start,
			    end = data.end,
			    $fat = el.$fat;


			$fat.text(start + '..' + end + ' %');
		}
	}, {
		key: 'change',
		value: function change(e) {
			var _e$data4 = e.data,
			    that = _e$data4.that,
			    el = _e$data4.el,
			    $form = el.$form,
			    formData = $form.data(),
			    data = {
				form: $form.serialize(),
				params: $.param(formData),
				url: $form.prop('action')
			};


			that.$window.trigger('menu:filterChange', data);
		}
	}]);

	return Menu;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Order = function () {
	function Order() {
		_classCallCheck(this, Order);

		this.selectors = {
			el: '.js-order',
			scale: '.js-scale',
			btn: '.js-button',
			footer: '.js-footer',
			summ: '.js-summ',
			totalSumm: '.js-total-summ',
			totalMass: '.js-total-mass',
			cartItem: '.js-cart-item',
			deliveryPrice: '.js-delivery-price',
			orderPayment: '.js-order-payment',
			// deliveryRest: '.js-delivery-rest',
			deliveryRestScale: '.js-scale',
			discount: {
				discount: '.js-order-discount',
				bonuses: '.js-order-discount-bonuses',
				card: '.js-order-discount-card',
				stock: '.js-order-discount-stock'
			}
		};

		this.classes = {
			fixed: 'order_fixed',
			sumErr: 'order_sum-err'
		};

		this.data = {
			payment: 'event-payment',
			minGoodsSum: 'min-goods-summ',
			cardDiscount: 'card-discount'
			// deliveryFree: 'delivery-free'
		};

		this.el = $(this.selectors.el);
		this.$window = $(window);
		this.$document = $(document);
		this.$footer = this.$document.find(this.selectors.footer);

		this.desktopWidth = 1000;

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data).init();
		}
	}

	_createClass(Order, [{
		key: 'get',
		value: function get(_ref, _ref2) {
			var scale = _ref.scale,
			    btn = _ref.btn,
			    summ = _ref.summ,
			    totalSumm = _ref.totalSumm,
			    totalMass = _ref.totalMass,
			    cartItem = _ref.cartItem,
			    deliveryPrice = _ref.deliveryPrice,
			    orderPayment = _ref.orderPayment,
			    discount = _ref.discount;
			var payment = _ref2.payment,
			    minGoodsSum = _ref2.minGoodsSum,
			    cardDiscount = _ref2.cardDiscount;

			// deliveryRest, deliveryRestScale
			this.elems = this.el.toArray().map(function (el) {
				var $el = $(el);
				var curSumm = parseFloat($el.find(summ).text().replace(',', '.').replace(/[^\d\.]/gi, ''));
				var minSumm = $el.data(minGoodsSum) || 0;
				return {
					$el: $el,
					$scale: $el.find(scale),
					$btn: $el.find(btn),
					$summ: $el.find(summ),
					$totalSumm: $el.find(totalSumm),
					$totalMass: $el.find(totalMass),
					$deliveryPrice: $el.find(deliveryPrice),
					$orderPayment: $el.find(orderPayment),
					discount: {
						$discount: $el.find(discount.discount),
						$bonuses: $el.find(discount.bonuses),
						$card: $el.find(discount.card),
						$stock: $el.find(discount.stock)
					},
					// $deliveryRest: $el.find(deliveryRest),
					// $deliveryRestScale: $el.find(deliveryRestScale),
					data: {
						payCard: true,
						eventPayment: $el.data(payment) || 'order:payment',
						cardDiscount: $el.data(cardDiscount) || 0,
						minGoodsSum: minSumm,
						bonuses: 0,
						promo: [],
						delivery: {},
						items: {},
						moreMinSum: curSumm > minSumm,
						cardDiscountSumm: 0,
						stock: $el.data('stock').stock || [],
						stockSum: $el.data('stock').stockSum || 0,
						promoDiscount: []
						// deliveryFree: $el.data(deliveryFree) || 100500
					}
				};
			});
			return this;
		}
	}, {
		key: 'init',
		value: function init() {
			var _this = this;

			this.elems.forEach(function (el) {
				_this.setPosition({ data: { that: _this, el: el } });

				_this.$window.on('resize, scroll', { that: _this, el: el }, _this.setPosition).on('cart:update', { that: _this, el: el }, _this.goodsChange).on('cart:delivery', { that: _this, el: el }, _this.deliveryChange).on('cart:bonuses', { that: _this, el: el }, _this.useBonuses).on('cart:submit', { el: el }, _this.disableBtn).on('cart:promo', { that: _this, el: el }, _this.applyPromo);

				el.$orderPayment.on('switch:change', { that: _this, el: el }, _this.paymentChange);
				el.$btn.on('click', { that: _this, el: el }, _this.validateForm);
			});
		}
	}, {
		key: 'disableBtn',
		value: function disableBtn(e, data) {
			e.data.el.$btn.prop('disabled', true);
		}
	}, {
		key: 'applyPromo',
		value: function applyPromo(e, data) {
			var _e$data = e.data,
			    that = _e$data.that,
			    el = _e$data.el,
			    type = data.type,
			    value = data.value,
			    percent = data.percent,
			    id = data.id,
			    remove = data.remove,
			    message = data.message;


			if (remove === undefined) {
				el.data.promo[id] = { type: type, value: value, percent: percent, message: message };
			} else {
				el.data.promo.splice(id, 1);
			}

			that.setDelivery(el, that);
			that.setTotal(el);
		}
	}, {
		key: 'validateForm',
		value: function validateForm(e) {
			var _e$data2 = e.data,
			    that = _e$data2.that,
			    el = _e$data2.el,
			    sumErr = that.classes.sumErr;


			if (el.data.moreMinSum) {
				e.preventDefault();
				that.$window.trigger('cart:validate');
			} else {
				console.log('Goods sum is to small');
			}

			el.$el.toggleClass(sumErr, !el.data.moreMinSum);
		}
	}, {
		key: 'useBonuses',
		value: function useBonuses(e, data) {
			var _e$data3 = e.data,
			    that = _e$data3.that,
			    el = _e$data3.el,
			    used = data.used;


			el.data.bonuses = used;
			that.setTotal(el);
		}
	}, {
		key: 'paymentChange',
		value: function paymentChange(e, data) {
			var _e$data4 = e.data,
			    that = _e$data4.that,
			    el = _e$data4.el,
			    value = data.value,
			    url = el.$el.data('url');


			el.data.payCard = value == 2 ? true : false;

			that.$window.trigger(el.data.eventPayment, { value: value });
			that.$window.trigger('ajax:get', { url: url, data: 'f_PaymentMethod=' + value, callback: that.paymentUpdate, context: { that: that, el: el } });
		}
	}, {
		key: 'paymentUpdate',
		value: function paymentUpdate(data) {
			var _data$context = data.context,
			    el = _data$context.el,
			    that = _data$context.that;


			that.goodsChange({ data: { that: that, el: el } }, data.data);
		}
	}, {
		key: 'goodsChange',
		value: function goodsChange(e, data) {
			var _e$data5 = e.data,
			    that = _e$data5.that,
			    el = _e$data5.el,
			    TotalItemPrice = data.TotalItemPrice,
			    TotalMass = data.TotalMass,
			    stock = data.stock,
			    stockSum = data.stockSum,
			    $summ = el.$summ,
			    $totalSumm = el.$totalSumm,
			    $totalMass = el.$totalMass;


			el.data.items.price = TotalItemPrice;
			el.data.stock = stock || [];
			el.data.stockSum = stockSum || 0;

			$summ.text(that.dot2comma(TotalItemPrice));
			$totalMass.text(TotalMass);

			that.$window.trigger('cart:deliveryRecalc');

			that.setTotal(el);
		}
	}, {
		key: 'deliveryChange',
		value: function deliveryChange(e, data) {
			var _e$data6 = e.data,
			    that = _e$data6.that,
			    el = _e$data6.el,
			    price = data.price,
			    deliveryFree = data.deliveryFree;


			el.data.delivery.price = price;
			el.data.delivery.free = deliveryFree;

			that.setDelivery(el, that);
		}
	}, {
		key: 'setDelivery',
		value: function setDelivery(el, that) {
			var $deliveryPrice = el.$deliveryPrice,
			    $deliveryRestScale = el.$deliveryRestScale,
			    _el$data$delivery = el.data.delivery,
			    price = _el$data$delivery.price,
			    free = _el$data$delivery.free;
			// $deliveryRest,
			// rest = (free > price) ? (free - price) : 0;

			var total = price;

			el.data.promo.forEach(function (el, i) {
				if (el.type == 'delivery') {
					total = 0;
				}
			});

			el.data.delivery.total = total;

			$deliveryPrice.text(total);
			// $deliveryRest.text(total === 0 ? 0 : rest);
			// $deliveryRestScale.trigger('scale:change', {total: free, value: free - rest});

			that.setTotal(el);
		}
	}, {
		key: 'setDiscount',
		value: function setDiscount(el) {
			var _this2 = this;

			var discount = el.discount,
			    _el$data = el.data,
			    bonuses = _el$data.bonuses,
			    cardDiscountSumm = _el$data.cardDiscountSumm,
			    stock = _el$data.stock,
			    stockSum = _el$data.stockSum,
			    promoDiscount = _el$data.promoDiscount,
			    promoTotal = promoDiscount.reduce(function (sum, cur) {
				return sum + cur.sum;
			}, 0),
			    total = bonuses + cardDiscountSumm + stockSum + promoTotal,
			    allStock = stock.concat(promoDiscount);

			var html = "";

			discount.$bonuses.text((bonuses > 0 ? "−" : "") + this.dot2comma(bonuses));
			discount.$card.text((cardDiscountSumm > 0 ? "−" : "") + this.dot2comma(cardDiscountSumm));

			if (allStock.length > 0) {
				allStock.forEach(function (item) {
					html += '\n\t\t\t\t\t<div class="order__discount-block">\n\t\t\t\t\t\t<div class="order__discount-type">' + item.name + '\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class="order__discount-amount">' + ((item.sum > 0 ? "−" : "") + _this2.dot2comma(item.sum)) + ' \u20BD\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t';
				});
				discount.$stock.html(html);
			}

			discount.$discount.text((total > 0 ? "−" : "") + this.dot2comma(total));
		}
	}, {
		key: 'setTotal',
		value: function setTotal(el) {
			var delivery = el.data.delivery.total || parseFloat(el.$deliveryPrice.text().replace(' ', '')),
			    items = el.data.items.price || parseFloat(el.$summ.text().replace(' ', '')),
			    total = items - el.data.bonuses,
			    cardDiscount = el.data.cardDiscount;


			el.data.moreMinSum = items >= el.data.minGoodsSum;

			el.data.promoDiscount = [];

			el.data.promo.forEach(function (item, i) {
				if (item.type == 'discount') {
					var discount = 0;
					if (item.percent !== undefined) {
						discount = total * (item.value / 100);
						total -= discount;
					} else {
						discount = item.value;
						total -= item.value;
					}

					el.data.promoDiscount.push({
						name: item.message,
						sum: discount
					});
				}
			});

			if (el.data.payCard) {
				el.data.cardDiscountSumm = this.rounding(total * cardDiscount / 100);

				total = total - el.data.cardDiscountSumm;
			} else {
				el.data.cardDiscountSumm = 0;
			}

			el.$totalSumm.text(this.dot2comma(total + delivery));

			this.setDiscount(el);
		}
	}, {
		key: 'rounding',
		value: function rounding(num) {
			return Number.parseFloat(num.toFixed(2).toString());
		}
	}, {
		key: 'dot2comma',
		value: function dot2comma(num) {
			return num.toString().replace('.', ',');
		}
	}, {
		key: 'setPosition',
		value: function setPosition(e) {
			var _e$data7 = e.data,
			    that = _e$data7.that,
			    el = _e$data7.el,
			    fixed = that.classes.fixed,
			    isMobile = that.$window.width() < that.desktopWidth;


			if (isMobile) {
				el.$el.removeClass(fixed);
			} else {
				var elHeigth = el.$el.height(),
				    dHeight = that.$document.height(),
				    wHeight = that.$window.height(),
				    scrollPos = that.$document.scrollTop(),
				    footerH = that.$footer.height();

				var pos = dHeight - wHeight - scrollPos - footerH,
				    hide = pos > elHeigth;

				el.$el.toggleClass(fixed, hide);
			}
		}
	}]);

	return Order;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var PrivatePage = function () {
	function PrivatePage() {
		_classCallCheck(this, PrivatePage);

		this.selectors = {
			el: '.js-private-page',
			form: '.js-private-page-form',
			resume: '.js-private-page-resume',
			changes: '.js-private-page-changes',
			saveBtn: '.js-private-page-save-btn',
			infoFields: '.js-private-page-info .js-field',
			addr: '.js-saved-addr',
			history: '.js-private-page-history',
			historyHide: '.js-private-page-history-hide',

			footer: '.js-footer'
		};

		this.classes = {
			fixed: 'private-page__resume_fixed',
			changes: 'private-page_changes',
			historyHide: 'private-page__history_hide'
		};

		this.data = {};

		this.el = $(this.selectors.el);
		this.$window = $(window);
		this.$document = $(document);
		this.$footer = this.$document.find(this.selectors.footer);

		this.desktopWidth = 1000;

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data).init();
		}
	}

	_createClass(PrivatePage, [{
		key: 'get',
		value: function get(_ref, data) {
			var form = _ref.form,
			    resume = _ref.resume,
			    changes = _ref.changes,
			    saveBtn = _ref.saveBtn,
			    infoFields = _ref.infoFields,
			    addr = _ref.addr,
			    history = _ref.history,
			    historyHide = _ref.historyHide;

			this.elems = this.el.toArray().map(function (el) {
				var $el = $(el);
				return {
					$el: $el,
					$form: $el.find(form),
					$resume: $el.find(resume),
					$changes: $el.find(changes),
					$saveBtn: $el.find(saveBtn),
					$infoFields: $el.find(infoFields),
					$addr: $el.find(addr),
					$history: $el.find(history),
					$historyBtn: $el.find(historyHide),
					data: {
						changedEls: {}
					}
				};
			});
			return this;
		}
	}, {
		key: 'init',
		value: function init() {
			var _this = this;

			this.elems.forEach(function (el) {
				_this.setResumePosition({ data: { that: _this, el: el } });

				// this.$window
				// .on('resize, scroll', {that: this, el}, this.setResumePosition);

				el.$infoFields.on('field:change', { that: _this, el: el }, _this.infoChange);
				el.$addr.on('savedAddr:delete', { that: _this, el: el }, _this.infoChange);
				el.$historyBtn.on('click', { that: _this, el: el }, _this.hideHistory);
				el.$saveBtn.on('click', { el: el }, _this.sendForm);
			});
		}
	}, {
		key: 'sendForm',
		value: function sendForm(e) {
			var el = e.data.el;


			el.$form.submit();
		}
	}, {
		key: 'hideHistory',
		value: function hideHistory(e) {
			var _e$data = e.data,
			    that = _e$data.that,
			    el = _e$data.el,
			    historyHide = that.classes.historyHide;


			el.$history.toggleClass(historyHide);
		}
	}, {
		key: 'infoChange',
		value: function infoChange(e, data) {
			var _e$data2 = e.data,
			    that = _e$data2.that,
			    el = _e$data2.el,
			    value = data.value,
			    label = data.label,
			    addr = data.addr;


			el.data.changedEls[label] = value;
			that.showChanges(el);
		}
	}, {
		key: 'showChanges',
		value: function showChanges(el) {
			var changes = this.classes.changes,
			    changedEls = el.data.changedEls;


			if (!jQuery.isEmptyObject(changedEls)) {
				var text = '';
				for (var key in changedEls) {
					text += key + ' \u2013 ' + changedEls[key] + ', ';
				}

				el.$changes.text(text.slice(0, -2));
				el.$el.addClass(changes);

				Stickyfill.add(el.$resume);
			}
		}
	}, {
		key: 'setResumePosition',
		value: function setResumePosition(e) {
			var _e$data3 = e.data,
			    that = _e$data3.that,
			    el = _e$data3.el,
			    fixed = that.classes.fixed,
			    isMobile = that.$window.width() < that.desktopWidth;


			if (isMobile) {
				el.$resume.removeClass(fixed);
			} else {
				var elHeigth = el.$resume.height(),
				    dHeight = that.$document.height(),
				    wHeight = that.$window.height(),
				    scrollPos = that.$document.scrollTop(),
				    footerH = that.$footer.height(),
				    pos = dHeight - wHeight - scrollPos - footerH,
				    hide = pos > elHeigth;

				el.$resume.toggleClass(fixed, hide);
			}
		}
	}]);

	return PrivatePage;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Product = function () {
	function Product() {
		_classCallCheck(this, Product);

		this.selectors = {
			el: '.js-product',
			add: '.js-product-add',
			remind: '.js-product-remind',
			buttonText: '.js-product-button-text'
		};

		this.classes = {
			envelopeSuccess: 'product__name-envelope_success'
		};

		this.data = {
			url: 'url',
			eventAdd: 'event-add'
		};
		this.el = $(this.selectors.el);
		this.$window = $(window);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data).init();
		}
	}

	_createClass(Product, [{
		key: 'get',
		value: function get(_ref, _ref2) {
			var add = _ref.add,
			    remind = _ref.remind,
			    buttonText = _ref.buttonText;
			var eventAdd = _ref2.eventAdd,
			    url = _ref2.url;

			this.elems = this.el.toArray().map(function (el) {
				var $el = $(el);
				return {
					$el: $el,
					$add: $el.find(add),
					$remind: $el.find(remind),
					$buttonText: $el.find(buttonText),
					data: {
						url: $el.data(url),
						eventAdd: $el.data(eventAdd) || 'cart:add'
					}
				};
			});
			return this;
		}
	}, {
		key: 'init',
		value: function init() {
			var _this = this;

			this.$window.on('product:reload', { that: this }, this.reload);

			this.elems.forEach(function (el) {
				el.$add.on('click', { that: _this, el: el }, _this.addToCart);
				el.$remind.on('click', { that: _this, el: el }, _this.remindWhenArrive);
				el.$remind.data() && el.$remind.data().success && _this.toggleRemindButton({ that: _this, el: el });
			});
		}
	}, {
		key: 'reload',
		value: function reload(e) {
			var that = e.data.that;


			that.$window.off('product:reload', this.reload);
			that.elems = [];
			that.el = $(that.selectors.el);

			that.get(that.selectors, that.data).init();
		}
	}, {
		key: 'addToCart',
		value: function addToCart(e) {
			e.preventDefault();

			var _e$data = e.data,
			    that = _e$data.that,
			    el = _e$data.el,
			    $add = el.$add,
			    url = el.data.url,
			    data = $add.data(),
			    callback = that.addResponce,
			    context = { that: that, el: el };


			that.$window.trigger('ajax:get', { url: url, data: $.param(data), callback: callback, context: context });
		}
	}, {
		key: 'remindWhenArrive',
		value: function remindWhenArrive(e) {
			e.preventDefault();

			var _e$data2 = e.data,
			    that = _e$data2.that,
			    el = _e$data2.el,
			    $remind = el.$remind,
			    data = $remind.data();


			el.$remind.prop('disabled', true);

			$.ajax({ type: 'POST', url: '/netcat/modules/default/actions/item_arrived_notifier.php', data: $.param(data) }).done(function () {
				that.toggleRemindButton({ that: that, el: el });
			}).fail(function () {
				$('.subscribe-overlay').data('cc', data.cc).data('msg', data.msg).toggleClass('subscribe-overlay_open', true);
				console.log($('.subscribe-overlay').data());
			});
		}
	}, {
		key: 'toggleRemindButton',
		value: function toggleRemindButton(data) {
			var that = data.that,
			    el = data.el;


			el.$remind.toggleClass(that.classes.envelopeSuccess);
			if (el.$remind.hasClass(that.classes.envelopeSuccess)) {
				el.$remind.off('click');
				el.$buttonText.text('Сообщим Вам\nо поступлении');
			} else {
				el.$remind.on('click', { that: that, el: el }, that.remindWhenArrive);
				el.$buttonText.text('Уведомить\nо поступлении');
			}
		}
	}, {
		key: 'addResponce',
		value: function addResponce(data) {
			var _data$context = data.context,
			    that = _data$context.that,
			    el = _data$context.el;


			that.$window.trigger(el.data.eventAdd, data.data);
		}
	}]);

	return Product;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _objectDestructuringEmpty(obj) { if (obj == null) throw new TypeError("Cannot destructure undefined"); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ProductPage = function () {
	function ProductPage() {
		_classCallCheck(this, ProductPage);

		this.selectors = {
			el: '.js-product-page',
			sticky: '.js-product-sticky',
			bill: '.js-product-page-bill',
			quantity: '.js-product-page-quantity',
			price: '.js-product-page-price',
			btn: '.js-product-page-btn',
			form: '.js-product-page-form'
		};

		this.data = {};

		this.el = $(this.selectors.el);
		this.$window = $(window);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data).init();
		}
	}

	_createClass(ProductPage, [{
		key: 'get',
		value: function get(_ref, _ref2) {
			var sticky = _ref.sticky,
			    bill = _ref.bill,
			    quantity = _ref.quantity,
			    price = _ref.price,
			    btn = _ref.btn,
			    form = _ref.form;

			_objectDestructuringEmpty(_ref2);

			this.elems = this.el.toArray().map(function (el) {
				var $el = $(el);
				return {
					$el: $el,
					$sticky: $el.find(sticky),
					$bill: $el.find(bill),
					$quantity: $el.find(quantity),
					$btn: $el.find(btn),
					$form: $el.find(form),
					data: {
						price: $el.find(price).data('price')
					}
				};
			});
			return this;
		}
	}, {
		key: 'init',
		value: function init() {
			var _this = this;

			this.elems.forEach(function (el) {
				Stickyfill.add(el.$sticky);

				el.$bill.on('click', { that: _this }, _this.showBill);
				el.$quantity.on('counter:change', { that: _this, el: el }, _this.setQuantity);
				el.$btn.on('click', { that: _this, el: el }, _this.addToCart);
			});
		}
	}, {
		key: 'addToCart',
		value: function addToCart(e) {
			e.preventDefault();

			var _e$data = e.data,
			    el = _e$data.el,
			    that = _e$data.that,
			    $form = el.$form,
			    url = $form.prop('action'),
			    data = $form.serialize(),
			    callback = that.cartUpdate,
			    additional = $.param(el.$form.data());


			el.$btn.prop('disabled', true);

			that.$window.trigger('ajax:get', { url: url, data: data + '&' + additional, callback: callback, context: { that: that, el: el } });
		}
	}, {
		key: 'cartUpdate',
		value: function cartUpdate(_ref3) {
			var data = _ref3.data,
			    context = _ref3.context;
			var that = context.that,
			    el = context.el;


			el.$btn.prop('disabled', false);

			that.$window.trigger('cart:update', data);
		}
	}, {
		key: 'setQuantity',
		value: function setQuantity(e, data) {
			var _e$data2 = e.data,
			    el = _e$data2.el,
			    that = _e$data2.that,
			    value = data.result;


			var summ = Number.parseFloat((el.data.price * value).toFixed(2).toString()).toString().replace('.', ',');

			el.$btn.text('\u041A\u0443\u043F\u0438\u0442\u044C \u0437\u0430 ' + summ + ' \u20BD');

			that.$window.trigger('bill:amount', { value: value });
		}
	}, {
		key: 'showBill',
		value: function showBill(e) {
			var that = e.data.that;


			that.$window.trigger('bill:show');
		}
	}]);

	return ProductPage;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Promo = function () {
	function Promo() {
		_classCallCheck(this, Promo);

		this.selectors = {
			el: '.js-promo',
			add: '.js-promo-add',
			field: '.js-field',
			wrap: '.js-promo-wrap',
			fieldWrap: '.js-promo-field'
		};

		this.classes = {
			active: 'promo_active'
		};

		this.data = {
			url: 'validate-url',
			eventValid: 'event-valid'
		};

		this.el = $(this.selectors.el);
		this.$window = $(window);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data).init();
		}
	}

	_createClass(Promo, [{
		key: 'get',
		value: function get(_ref, _ref2) {
			var add = _ref.add,
			    field = _ref.field,
			    wrap = _ref.wrap,
			    fieldWrap = _ref.fieldWrap;
			var url = _ref2.url,
			    eventValid = _ref2.eventValid;

			this.elems = this.el.toArray().map(function (el) {
				var $el = $(el);
				return {
					$el: $el,
					$field: $el.find(field),
					$add: $el.find(add),
					$wrap: $el.find(wrap),
					$fieldWrap: $el.find(fieldWrap),
					data: {
						id: 0,
						url: $el.data(url),
						eventValid: $el.data(eventValid) || 'cart:promo',
						email: false
					}
				};
			});
			return this;
		}
	}, {
		key: 'init',
		value: function init() {
			var _this = this;

			this.elems.forEach(function (el) {
				_this.$window.on('cart:email', { that: _this, el: el }, _this.setEmail);
				el.$add.on('click', { that: _this, el: el }, _this.add);
			});
		}
	}, {
		key: 'setEmail',
		value: function setEmail(e, data) {
			var _e$data = e.data,
			    that = _e$data.that,
			    el = _e$data.el,
			    email = data.email;


			el.data.email = email;
		}
	}, {
		key: 'add',
		value: function add(e) {
			e.preventDefault();
			var _e$data2 = e.data,
			    that = _e$data2.that,
			    el = _e$data2.el,
			    _that$classes = that.classes,
			    active = _that$classes.active,
			    idPref = _that$classes.idPref,
			    field = that.selectors.field,
			    id = el.data.id += 1;


			el.$el.addClass(active);

			var $newEl = el.$field.clone();
			el.$wrap.append($newEl);

			$newEl.data('id', id).find('.field__label').text('\u041F\u0440\u043E\u043C\u043E\u043A\u043E\u0434 #' + id);

			that.$window.trigger('field:add', { el: $newEl });
			$newEl.on('field:input', { that: that, el: el, target: $newEl }, that.change).on('field:clear', { that: that, el: el, target: $newEl }, that.hideFields).find('.js-field-input').prop('disabled', false).prop('id', 'promo' + id);
		}
	}, {
		key: 'hideFields',
		value: function hideFields(e) {
			var _e$data3 = e.data,
			    that = _e$data3.that,
			    el = _e$data3.el,
			    eventValid = el.data.eventValid,
			    $cur = $(e.currentTarget),
			    active = that.classes.active,
			    id = $cur.data('id');


			that.$window.trigger(eventValid, { id: id, remove: true });
			$cur.remove();
			el.$el.toggleClass(active, el.$wrap.children().length > 1);
		}
	}, {
		key: 'change',
		value: function change(e, data) {
			var _e$data4 = e.data,
			    that = _e$data4.that,
			    el = _e$data4.el,
			    target = _e$data4.target,
			    value = data.value;


			if (el.data.timer) {
				clearTimeout(el.data.timer);
			}
			that.$window.trigger('cart:getEmail');
			el.data.timer = setTimeout(that.validatePromo, 2000, { that: that, target: target, el: el, value: value });
		}
	}, {
		key: 'validatePromo',
		value: function validatePromo(data) {
			var that = data.that,
			    el = data.el,
			    target = data.target,
			    value = data.value,
			    url = el.data.url,
			    callback = that.setAnswer,
			    context = { that: that, el: el, target: target },
			    field = target.find('.js-field-input');

			var formData = {};

			if (el.data.email) {
				field.trigger('field:extra', {
					show: false
				});
				formData = {
					email: el.data.email
				};
				formData[field.prop('name')] = field.val();

				that.$window.trigger('ajax:get', { url: url, data: formData, callback: callback, context: context });
			} else {
				field.trigger('field:extra', {
					text: 'Введите электроную почту и повторите ввод',
					show: true
				});
			}
		}
	}, {
		key: 'setAnswer',
		value: function setAnswer(data) {
			var _data$context = data.context,
			    that = _data$context.that,
			    el = _data$context.el,
			    target = _data$context.target,
			    eventValid = el.data.eventValid,
			    id = target.data('id'),
			    _data$data = data.data,
			    error = _data$data.error,
			    valid = _data$data.valid,
			    type = _data$data.type,
			    value = _data$data.value,
			    percent = _data$data.percent,
			    message = _data$data.message,
			    error_message = _data$data.error_message;


			if (error !== undefined || valid === false) {
				var text = error_message || 'Неверный промокод';

				var _data = {
					show: true,
					text: text
				};
				target.trigger('field:error', _data);
			} else {
				var _data2 = {
					show: true,
					text: message || 'Верный промокод'
				};

				target.trigger('field:disable', { disabled: true }).trigger('field:extra', _data2);
				that.$window.trigger(eventValid, { type: type, value: value, percent: percent, id: id, message: message });
			}
		}
	}]);

	return Promo;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Purchase = function () {
	function Purchase() {
		_classCallCheck(this, Purchase);

		this.selectors = {
			el: '.js-purchase',
			repeat: '.js-purchase-repeat',
			form: '.js-purchase-form',
			more: '.js-purchase-more'
		};

		this.classes = {
			more: 'purchase_more'
		};

		this.data = {};

		this.el = $(this.selectors.el);
		this.$window = $(window);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data).init();
		}
	}

	_createClass(Purchase, [{
		key: 'get',
		value: function get(_ref, data) {
			var repeat = _ref.repeat,
			    form = _ref.form,
			    more = _ref.more;

			this.elems = this.el.toArray().map(function (el) {
				var $el = $(el);
				return {
					$el: $el,
					$repeat: $el.find(repeat),
					$form: $el.find(form),
					$more: $el.find(more),
					data: {}
				};
			});
			return this;
		}
	}, {
		key: 'init',
		value: function init() {
			var _this = this;

			this.elems.forEach(function (el) {
				el.$repeat.on('click', { that: _this, el: el }, _this.sendForm);
				el.$more.on('click', { that: _this, el: el }, _this.more);
			});
		}
	}, {
		key: 'sendForm',
		value: function sendForm(e) {
			var _e$data = e.data,
			    that = _e$data.that,
			    el = _e$data.el;


			el.$form.submit();
		}
	}, {
		key: 'more',
		value: function more(e) {
			var _e$data2 = e.data,
			    that = _e$data2.that,
			    el = _e$data2.el;


			el.$el.addClass(that.classes.more);
		}
	}]);

	return Purchase;
}();
'use strict';

$(document).ready(function () {
	$('.recipe-page__img-item').on('click', function () {
		var thisItem = $(this).data('item');

		$('.recipe-page__img-item').removeClass('active');
		$('.recipe-page__img').removeClass('active');

		$(this).addClass('active');
		$('.recipe-page__img#' + thisItem).addClass('active');
	});

	$('.recipe-page__img').on('click', function () {
		var thisItem = $('.recipe-page__img-item.active').data('item');
		var firstItem = $('.recipe-page__img-item').eq(0).data('item');
		var lastItem = $('.recipe-page__img-item').last().data('item');

		$('.recipe-page__img-item').removeClass('active');
		$('.recipe-page__img').removeClass('active');

		if (!(thisItem == lastItem)) {
			$('.recipe-page__img-item[data-item=' + thisItem + ']').next().addClass('active');
			$('.recipe-page__img#' + thisItem).next().addClass('active');
		} else {
			$('.recipe-page__img-item[data-item=' + firstItem + ']').addClass('active');
			$('.recipe-page__img#' + firstItem).addClass('active');
		}
	});
});
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var SavedAddr = function () {
	function SavedAddr() {
		_classCallCheck(this, SavedAddr);

		this.selectors = {
			el: '.js-saved-addr',
			field: '.js-field'
		};

		this.classes = {};

		this.data = {
			eventDelete: 'event-delete'
		};

		this.el = $(this.selectors.el);
		this.$window = $(window);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data).init();
		}
	}

	_createClass(SavedAddr, [{
		key: 'get',
		value: function get(_ref, _ref2) {
			var field = _ref.field;
			var eventDelete = _ref2.eventDelete;

			this.elems = this.el.toArray().map(function (el) {
				var $el = $(el);
				return {
					$el: $el,
					$field: $el.find(field),
					data: {
						eventDelete: $el.data(eventDelete) || 'savedAddr:delete'
					}
				};
			});
			return this;
		}
	}, {
		key: 'init',
		value: function init() {
			var _this = this;

			this.elems.forEach(function (el) {
				el.$field.on('field:clear', { that: _this, el: el }, _this.fieldDelete);
			});
		}
	}, {
		key: 'fieldDelete',
		value: function fieldDelete(e, _ref3) {
			var value = _ref3.value,
			    label = _ref3.label;
			var _e$data = e.data,
			    that = _e$data.that,
			    el = _e$data.el,
			    eventDelete = el.data.eventDelete;


			$(e.currentTarget).parent().remove();

			el.$el.trigger(eventDelete, { value: value, label: label });
		}

		// setSuggestion(e, data) {
		// 	const	{that, el} = e.data;

		// 	el.data.suggestions = el.$address.suggestions({
		// 		token: "670831253184a6570dcfb29298d37c4800de6d75",
		// 		type: "ADDRESS",
		// 		constraints: [
		// 			{locations: {kladr_id: 50}},
		// 			{locations: {kladr_id: 77}},
		// 			{locations: {kladr_id: 47}},
		// 			{locations: {kladr_id: 78}}
		// 		],
		// 		onSelect: (suggestion) => {
		// 			console.log(suggestion);
		// 			// that.getDeliveryPrice(el);
		// 		}
		// 	}).suggestions();
		// }

	}]);

	return SavedAddr;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Scale = function () {
	function Scale() {
		_classCallCheck(this, Scale);

		this.selectors = {
			el: '.js-scale',
			value: '.js-scale-value',
			slider: '.js-scale-slider'
		};

		this.classes = {};

		this.data = {
			change: 'event-change'
		};

		this.el = $(this.selectors.el);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data).init();
		}
	}

	_createClass(Scale, [{
		key: 'get',
		value: function get(_ref, _ref2) {
			var value = _ref.value,
			    slider = _ref.slider;
			var change = _ref2.change;

			this.elems = this.el.toArray().map(function (el) {
				var $el = $(el);
				return {
					$el: $el,
					$value: $el.find(value),
					$slider: $el.find(slider),
					data: {
						change: $el.data(change) || 'scale:change'
					}
				};
			});
			return this;
		}
	}, {
		key: 'init',
		value: function init() {
			var _this = this;

			this.elems.forEach(function (el) {
				el.$el.on(el.data.change, { that: _this, el: el }, _this.change);
			});
		}
	}, {
		key: 'change',
		value: function change(e, data) {
			var _e$data = e.data,
			    that = _e$data.that,
			    el = _e$data.el,
			    total = data.total,
			    value = data.value;

			var perc = value / total * 100;

			perc = perc > 100 ? '100%' : perc + '%';

			el.$value.css('width', perc);
			el.$slider.css('margin-left', perc);
		}
	}]);

	return Scale;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Select = function () {
	function Select() {
		_classCallCheck(this, Select);

		this.selectors = {
			select: '.js-select',
			native: '.js-select-native',
			sub: '.js-select-sub',
			list: '.js-select-list',
			option: '.js-select-option',
			text: '.js-select-text'
		};

		this.classes = {
			open: 'select_open',
			selected: 'select__option_selected'
		};

		this.el = $(this.selectors.select);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors).init();
		}
	}

	_createClass(Select, [{
		key: 'get',
		value: function get(_ref) {
			var native = _ref.native,
			    sub = _ref.sub,
			    list = _ref.list,
			    option = _ref.option,
			    text = _ref.text;

			this.elems = this.el.toArray().map(function (el) {
				var $el = $(el);
				return {
					$el: $el,
					$native: $el.find(native),
					$sub: $el.find(sub),
					$list: $el.find(list),
					$options: $el.find(option),
					$text: $el.find(text)
				};
			});
			return this;
		}
	}, {
		key: 'init',
		value: function init() {
			var _this = this;

			this.elems.forEach(function (elem) {
				elem.$sub.on('click', { elem: elem, that: _this }, _this.toggle);
				elem.$options.on('click', { elem: elem, that: _this }, _this.select);
			});
		}
	}, {
		key: 'toggle',
		value: function toggle(e) {
			var _e$data = e.data,
			    elem = _e$data.elem,
			    that = _e$data.that;

			var open = e.data.open || !elem.$el.hasClass(that.classes.open);

			elem.$el.toggleClass(that.classes.open, open);

			if (open) {
				$('body').on('click', { elem: elem, that: that }, that.checkCloseClick);
			} else {
				$('body').off('click', that.checkCloseClick);
			}
		}
	}, {
		key: 'select',
		value: function select(e) {
			var _e$data2 = e.data,
			    elem = _e$data2.elem,
			    that = _e$data2.that;

			var $selected = $(e.currentTarget);
			var value = $selected.data('value');
			var id = parseInt($selected.data('id'));

			elem.$text.text($selected.text());
			elem.$native.val(value);
			that.setSelected(elem.$options, id, that.classes.selected);
			that.toggle({
				data: {
					elem: elem,
					that: that,
					open: false
				}
			});
		}
	}, {
		key: 'checkCloseClick',
		value: function checkCloseClick(e) {
			var _e$data3 = e.data,
			    elem = _e$data3.elem,
			    that = _e$data3.that,
			    notSelect = !$(e.target).is('.select__option, .select__text, .select__icon, .select__wrap');


			if (notSelect) {
				that.toggle({ data: { elem: elem, that: that } });
			}
		}
	}, {
		key: 'setSelected',
		value: function setSelected($options, id, className) {
			$options.each(function (i, el) {
				$(el).toggleClass(className, id === i);
			});
		}
	}]);

	return Select;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Slider = function () {
	function Slider() {
		_classCallCheck(this, Slider);

		this.selectors = {
			el: '.js-slider',
			slider: '.js-slider-wrap',
			start: '.js-slider-start',
			end: '.js-slider-end'
		};

		this.classes = {};

		this.data = {
			min: 'min',
			max: 'max',
			start: 'start',
			end: 'end',
			eventChange: 'event-change',
			eventUpdate: 'event-update'
		};

		this.el = $(this.selectors.el);
		// this.$window = $(window);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data).init();
		}
	}

	_createClass(Slider, [{
		key: 'get',
		value: function get(_ref, _ref2) {
			var slider = _ref.slider,
			    start = _ref.start,
			    end = _ref.end;
			var min = _ref2.min,
			    max = _ref2.max,
			    startData = _ref2.start,
			    endData = _ref2.end,
			    eventChange = _ref2.eventChange,
			    eventUpdate = _ref2.eventUpdate;

			this.elems = this.el.toArray().map(function (el) {
				var $el = $(el);
				return {
					$el: $el,
					$slider: $el.find(slider),
					$start: $el.find(start),
					$end: $el.find(end),
					data: {
						min: $el.data(min),
						max: $el.data(max),
						start: $el.data(startData),
						end: $el.data(endData),
						eventChange: $el.data(eventChange) || 'slider:change',
						eventUpdate: $el.data(eventUpdate) || 'slider:update'
					}
				};
			});
			return this;
		}
	}, {
		key: 'init',
		value: function init() {
			var _this = this;

			this.elems.forEach(function (el) {
				var slider = el.$slider[0];
				_this.setup(el);

				slider.noUiSlider.on('change', function (data) {
					return _this.change(el, data);
				});
				slider.noUiSlider.on('update', function (data) {
					return _this.update(el, data);
				});
			});
		}
	}, {
		key: 'change',
		value: function change(el, data) {
			var $start = el.$start,
			    $end = el.$end;


			$start.val(data[0]);
			$end.val(data[1]);

			el.$el.trigger(el.data.eventChange, { start: data[0], end: data[1] });
		}
	}, {
		key: 'update',
		value: function update(el, data) {
			var $start = el.$start,
			    $end = el.$end;


			$start.val(data[0]);
			$end.val(data[1]);

			el.$el.trigger(el.data.eventUpdate, { start: data[0], end: data[1] });
		}
	}, {
		key: 'setup',
		value: function setup(el) {
			var slider = el.$slider[0],
			    _el$data = el.data,
			    min = _el$data.min,
			    max = _el$data.max,
			    start = _el$data.start,
			    end = _el$data.end;


			noUiSlider.create(slider, {
				start: [start, end],
				step: 1,
				connect: true,
				range: { min: min, max: max },
				format: {
					to: function to(val) {
						return val;
					},
					from: function from(val) {
						return val;
					}
				}
			});
		}
	}]);

	return Slider;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var SubscribeOverlay = function () {
	function SubscribeOverlay() {
		_classCallCheck(this, SubscribeOverlay);

		this.selectors = {
			el: '.js-subscribe-overlay',
			form: '.js-form',
			container: '.js-container'
		};

		this.classes = {
			open: 'subscribe-overlay_open'
		};

		this.data = {
			cc: 'cc',
			msg: 'msg'
		};

		this.$window = $(window);
		this.el = $(this.selectors.el);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data).init();
		}
	}

	_createClass(SubscribeOverlay, [{
		key: 'get',
		value: function get(_ref, _ref2) {
			var form = _ref.form,
			    container = _ref.container;
			var cc = _ref2.cc,
			    msg = _ref2.msg;

			this.elems = this.el.toArray().map(function (el) {
				var $el = $(el);
				return {
					$el: $el,
					$form: $el.find(form),
					$container: $el.find(container),
					data: {
						cc: $el.data(cc),
						msg: $el.data(msg)
					}
				};
			});
			return this;
		}
	}, {
		key: 'init',
		value: function init() {
			var _this = this;

			this.elems.forEach(function (elem) {
				elem.$el.on('click', { elem: elem, that: _this }, _this.toggle);
				elem.$form.on('submit', { elem: elem, that: _this }, _this.subscribe);
			});
		}
	}, {
		key: 'toggle',
		value: function toggle(e, isForce) {
			var _e$data = e.data,
			    elem = _e$data.elem,
			    that = _e$data.that;

			var open = e.data.open || !elem.$el.hasClass(that.classes.open);
			if ($(e.target).is(that.selectors.el) || isForce) {
				elem.$el.toggleClass(that.classes.open, open);
			};
		}
	}, {
		key: 'subscribe',
		value: function subscribe(e) {
			var _e$data2 = e.data,
			    elem = _e$data2.elem,
			    that = _e$data2.that;

			e.preventDefault();
			var formData = $(this).serialize() + '&' + $.param(that.el.data());

			that.$window.trigger('ajax:get', { type: 'POST', url: '/netcat/modules/default/actions/item_arrived_notifier.php', data: formData });

			that.toggle({
				data: {
					elem: elem,
					that: that,
					open: false
				}
			}, true);
		}
	}]);

	return SubscribeOverlay;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Switch = function () {
	function Switch() {
		_classCallCheck(this, Switch);

		this.selectors = {
			el: '.js-switch',
			native: '.js-switch-native',
			item: '.js-switch-item'
		};

		this.classes = {
			selected: 'switch__item_selected'
		};

		this.data = {
			eventChange: 'event-change',
			eventGet: 'event-get'
		};

		this.el = $(this.selectors.el);
		this.$window = $(window);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data).init();
		}
	}

	_createClass(Switch, [{
		key: 'get',
		value: function get(_ref, _ref2) {
			var native = _ref.native,
			    item = _ref.item;
			var eventChange = _ref2.eventChange,
			    eventGet = _ref2.eventGet;

			this.elems = this.el.toArray().map(function (el) {
				var $el = $(el);
				return {
					$el: $el,
					$native: $el.find(native),
					$items: $el.find(item),
					data: {
						change: $el.data(eventChange) || 'switch:change',
						get: $el.data(eventGet) || 'switch:get'
					}
				};
			});
			return this;
		}
	}, {
		key: 'init',
		value: function init() {
			var _this = this;

			this.elems.forEach(function (elem) {
				elem.$el.on('switch:update', { that: _this, elem: elem }, _this.setActive);

				elem.$items.on('click', { that: _this, elem: elem }, _this.change);
				elem.$el.on(elem.data.get, { that: _this, elem: elem }, _this.getData);
			});
		}
	}, {
		key: 'setActive',
		value: function setActive(e, data) {
			var _e$data = e.data,
			    that = _e$data.that,
			    elem = _e$data.elem,
			    id = data.id;


			that.change({ data: { that: that, elem: elem } }, { id: id });
		}
	}, {
		key: 'getData',
		value: function getData(e) {
			var _e$data2 = e.data,
			    elem = _e$data2.elem,
			    that = _e$data2.that,
			    $active = elem.$native.filter('input:checked'),
			    value = $active.val(),
			    data = $active.data();


			elem.$el.trigger(elem.data.change, { value: value, data: data });
		}
	}, {
		key: 'change',
		value: function change(e, activeId) {
			var _e$data3 = e.data,
			    elem = _e$data3.elem,
			    that = _e$data3.that,
			    sel = that.classes.selected;

			var id = void 0;

			if (activeId !== undefined) {
				id = activeId.id;
			} else {
				id = $(e.currentTarget).data('id');
			}

			elem.$native.each(function (i, el) {
				var $el = $(el);

				$el.prop('checked', i == id);
				if (i == id) {
					var data = $el.data();
					elem.$el.trigger(elem.data.change, { value: $el.val(), data: data });
				}
			});
			elem.$items.each(function (i, el) {
				return $(el).toggleClass(sel, i == id);
			});
		}
	}]);

	return Switch;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var TextPage = function () {
	function TextPage() {
		_classCallCheck(this, TextPage);

		this.el = $('.text-page');

		if (this.el.length) {
			this.init();
		}
	}

	_createClass(TextPage, [{
		key: 'init',
		value: function init() {
			var headers = Array.from(document.querySelectorAll('.header_h2'));
			var anchors = Array.from(document.querySelectorAll('.sidebar__part'));

			for (var index = 0; index < headers.length; index++) {
				var parsedIndex = headers[index].getAttribute('id');
				anchors[index].setAttribute('onClick', 'textPage.jumpTo(\'#' + parsedIndex + '\')');
			}
		}
	}, {
		key: 'jumpTo',
		value: function jumpTo(header) {
			var offset = $(header).offset().top - 110;
			console.log(offset);
			$('html, body').animate({ scrollTop: offset }, 200, 'swing');
		}
	}]);

	return TextPage;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var AjaxHelper = function () {
	function AjaxHelper() {
		_classCallCheck(this, AjaxHelper);

		this.$window = $(window);
		this.init();
	}

	_createClass(AjaxHelper, [{
		key: 'change',
		value: function change(event, _ref) {
			var url = _ref.url,
			    _ref$type = _ref.type,
			    type = _ref$type === undefined ? 'GET' : _ref$type,
			    _ref$dataType = _ref.dataType,
			    dataType = _ref$dataType === undefined ? 'JSON' : _ref$dataType,
			    data = _ref.data,
			    callback = _ref.callback,
			    _ref$context = _ref.context,
			    context = _ref$context === undefined ? false : _ref$context;

			$.ajax({ url: url, type: type, data: data, dataType: dataType,
				success: function success(data) {
					if (typeof callback === 'function') {
						callback({ data: data, context: context });
					}
				}
			});
		}
	}, {
		key: 'init',
		value: function init() {
			this.$window.on('ajax:get', this.change);
		}
	}]);

	return AjaxHelper;
}();
"use strict";

var ajax = new AjaxHelper();

var field = new Field(),
    counter = new Counter(),
    select = new Select(),
    switchBlock = new Switch(),
    slider = new Slider(),
    head = new Head(),
    fieldOptions = new FieldOptions(),
    promo = new Promo(),
    comment = new Comment(),
    bonuses = new Bonuses(),
    scale = new Scale(),
    cartItem = new CartItem(),
    cartPage = new CartPage(),
    order = new Order(),
    purchase = new Purchase(),
    product = new Product(),
    menu = new Menu(),
    category = new Category(),
    savedAddr = new SavedAddr(),
    blogPage = new BlogPage(),
    catalogPage = new CatalogPage(),
    productPage = new ProductPage(),
    privatePage = new PrivatePage(),
    bill = new Bill(),
    changePass = new ChangePass(),
    friendPage = new FriendPage(),
    textPage = new TextPage(),
    subscribeOverlay = new SubscribeOverlay();
'use strict';

var frontpage = document.querySelector('[data-frontpage]');

frontpage && carouselSlider();

function carouselSlider() {

  var slider = document.querySelectorAll('.slider');
  slider.forEach(function (e, i) {
    var slider_list = e.querySelector('.slider__ul');
    var slide_left = e.querySelector('.slider__btn--prev');
    var slide_right = e.querySelector('.slider__btn--next');
    var count = slider_list.querySelectorAll('li').length;
    var clicks = 0;
    slide_right.addEventListener('click', moveLeft);

    function moveLeft() {
      clicks++;
      var x = 370;
      slide_left.style.display = 'block';
      slider_list.style.transform = 'translateX(' + (0 - x * clicks) + 'px)';
      if (count - clicks <= 3) {
        slide_right.style.display = 'none';
      }
    }

    slide_left.addEventListener('click', moveRight);

    function moveRight() {
      clicks--;
      var x = 370;
      slide_right.style.display = 'block';
      if (clicks == 0) {
        var y = 'translateX(' + 0 + 'px)';
      } else {
        var y = 'translateX(' + x * clicks * -1 + 'px)';
      }
      if (clicks == 3) {
        clicks = 0;
      }
      slider_list.style.transform = y;

      if (slider_list.style.transform == 'translateX(0px)') {
        slide_left.style.display = 'none';
      }
    }
  });

  $(document).ready(function () {

    $(".reviews__item").click(function () {
      event.preventDefault();
      $(".reviews__blockquote").hide();
      var BlockTrigger = $(this).data("review");
      $("#blockquote-" + BlockTrigger).show();
      $('.reviews__item').removeClass('reviews__item--active');
      $(this).addClass('reviews__item--active');

      if (BlockTrigger == 7) {
        $('.btn-block').hide();
      } else {
        $('.btn-block').show();
      }
    });
  });
};

frontpage && $(window).bind('scroll', function (e) {
  parallaxScroll();
});

function parallaxScroll() {
  var scrolled = $(window).scrollTop();
  $('.milk-big').css({
    'top': 480 + scrolled * .25 + 'px',
    'right': -131 + scrolled * .15 + 'px',
    'transform': 'rotate(-' + scrolled * .02 + 'deg)'
  });
  $('.milk-small').css({
    'top': 200 - scrolled * .5 + 'px',
    'left': 131 + scrolled * .25 + 'px',
    'transform': 'rotate(' + scrolled * .02 + 'deg)'
  });
  $('.butter').css({
    'top': 94 + scrolled * .55 + 'px',
    'left': 61 - scrolled * .25 + 'px',
    'transform': 'rotate(-' + scrolled * .02 + 'deg)'
  });
  $('.bottle').css({
    'top': 530 + scrolled * .3 + 'px',
    'left': -110 + scrolled * .25 + 'px',
    'transform': 'rotate(' + scrolled * .02 + 'deg)'
  });
  $('.cheese-big').css({
    'top': 87 - scrolled * .5 + 'px',
    'right': 0 + scrolled * .05 + 'px',
    'transform': 'rotate(-' + scrolled * .02 + 'deg)'
  });
  $('.cheese-small').css({
    'top': 679 - scrolled * .15 + 'px',
    'left': 487 - scrolled * .05 + 'px',
    'transform': 'rotate(' + scrolled * .02 + 'deg)'
  });
  $('.list').css({
    'top': -22 + scrolled * .55 + 'px',
    'left': 807 - scrolled * .55 + 'px',
    'transform': 'rotate(-' + scrolled * .02 + 'deg)'
  });
}