.product-page

	&__wrap
		+desktop()
			display flex
			flex-direction row
			align-items stretch
			min-height 100%

	// &__side
	// 	+desktop()
	// 		flex-basis 25%
	// 		box-shadow 2px 2px 2px 0 #ebe8c8
	// 		background-color #fffcda

	&__content
		// max-width 1080px
		margin 0 auto

	&__main
		padding 23px 0px 0
		+desktop()
			// flex-basis 75%
			padding 23px 0px 0

	&__general
		display flex
		flex-direction column-reverse
		padding 0 15px
		+desktop()
			flex-direction row
			flex-basis 75%
			padding 0 20px

		&-main
			flex 1

			padding-top 7px

			+desktop()
				padding-right 25px

		&-card
			position relative

			+desktop()
				flex 0 0 324px
				padding-right 12px

	&__card
		padding-bottom 30px
		+desktop()
			width 324px
			margin-bottom 20px
			padding-bottom 0

		+desktop()
			position -webkit-sticky
			position sticky
			top 85px
			width 100%
			margin-bottom 0

		&:after,
		&:before
			content ''
			display table

		&-desc
			display block
			font-family $trola
			fontSize(20, 28)
			color $black
			margin-bottom 5px

		&-name
			display block
			font-family $mulMed
			fontSize(35, 42)
			color $black

		&-quantity
			display flex
			flex-wrap wrap
			margin-bottom 27px

			&-price
				flex 1
				padding-bottom 4px

			&-counter
				display flex
				align-items flex-end
				flex-basis 150px
				margin-top 15px

		&-price

			&-old
				display table
				margin 17px 0px -30px 4px
				font-family $mulReg
				fontSize(14, 20)
				color $black
				&:after
					transform rotate(-15deg)
					content ""
					display block
					width 140%
					height 1px
					margin -30% 0 0 -20%
					background-color $red

			&-cur
				display block
				width 100%
				font-family $trola
				margin 39px 0 0 0
				fontSize(55, 36)
				color $black
				white-space nowrap

			&-weight
				display inline-block
				vertical-align top
				margin-left -5px
				font-family $mulReg
				fontSize(14, 20)
				color $black
				&:before
					content: '/ '

		&-button
			margin-bottom 13px

		&-legal-bill
			text-align center

			&-href
				display inline-block
				font-family $mulReg
				font-size 14px
				line-height 19px
				font-weight bold
				color $violet
				text-decoration none

	&__img
		display block
		width 100%
		height auto

		&-wrap
			clear both
			display block
			margin-bottom 32px
			+desktop()
				border-radius 5px

	&__paragraph p
		font-family $trola
		line-height 1.55
		font-size 20px
		margin 18px 0 0
		max-width 1000px
		&:last-child
			margin-bottom 60px

	&__info
		display flex
		flex-direction column
		margin-top 50px
		margin-bottom 50px
		padding-top 18px
		border-top 2px solid #000

		+desktop()
			flex-direction row

		&-text
			padding-top 5px
			margin-bottom 20px

			+desktop()
				flex 0 0 220px
				margin-bottom 0

			&-header
				width 160px
				font-family $mulMed
				fontSize(24, 29)
				font-weight 500
				
		
		&-content
			display flex
			flex-direction column
			flex 1

			&-text
				font-family $trola
				fontSize(20, 32)

			&-item
				position relative
				display flex
				justify-content space-between
				font-family $trola
				fontSize(20, 32)
				overflow hidden

				&:before
					content ". . . . . . . . . . . . . . . . . . . . " ". . . . . . . . . . . . . . . . . . . . " ". . . . . . . . . . . . . . . . . . . . " ". . . . . . . . . . . . . . . . . . . . " ". . . . . . . . . . . . . . . . . . . . " ". . . . . . . . . . . . . . . . . . . . "
					position absolute
					bottom 12px
					width 0
					height 2px
					white-space nowrap
					z-index -1
					font-size 11px
					line-height 2px
					letter-spacing 5px
					color rgba(0, 0, 0, 0.45)

				&-header
					padding-right 5px
					background rgb(251, 250, 240)

				&-description
					padding-left 5px
					background rgb(251, 250, 240)

	&__divider
		position relative
		text-align center
		user-select none
		margin-bottom 23px

		&-header
			display inline-block
			padding 10px 25px
			z-index 3
			font-family $mulMed
			fontSize(24, 29)
			background rgb(251, 250, 240)

			&:before
				content ''
				display block
				position absolute
				bottom 20px
				left 50%
				transform translateX(-50%)
				width 100%
				height 2px
				background #F1CB0D
				z-index -1

	&__delivery
		display flex
		flex-direction column
		margin-bottom 30px
		padding 0 10px

		+desktop()
			margin-bottom 70px
			padding 0
			flex-direction row

		&-col
			flex 1
			margin-bottom 20px

			+desktop()
				padding 0 30px
			
		&-header
			margin-bottom 16px
			font-family $mulMed
			fontSize(24, 29)
			font-weight 500
			
		&-description
			font-family $trola
			line-height 1.55
			font-size 20px
			margin 9px 0 18px

		&-ranges
			display flex
			flex-direction column
			flex 1
			margin-bottom 18px
			
			&-item
				position relative
				display flex
				justify-content space-between
				font-family $trola
				fontSize(20, 32)
				overflow hidden

				&:before
					content ". . . . . . . . . . . . . . . . . . . . " ". . . . . . . . . . . . . . . . . . . . " ". . . . . . . . . . . . . . . . . . . . " ". . . . . . . . . . . . . . . . . . . . " ". . . . . . . . . . . . . . . . . . . . " ". . . . . . . . . . . . . . . . . . . . "
					position absolute
					bottom 12px
					width 0
					height 2px
					white-space nowrap
					z-index -1
					font-size 11px
					line-height 2px
					letter-spacing 5px
					color rgba(0, 0, 0, 0.45)

				&-header
					padding-right 5px
					background rgb(251, 250, 240)

				&-description
					padding-left 5px
					background rgb(251, 250, 240)
					
		&-link
			font-family $trola
			line-height 1.55
			font-size 20px
			color #057fff
			text-decoration none
			margin 18px 0 0

	&__recommended
		margin 0 0 40px
		padding 0 15px
		font-size 0

		+desktop()
			margin 0 10px 40px
			padding 0 10px

		+desktop()
			padding 0

		&-el
			padding-bottom 30px
			+desktop()
				display inline-block
				width 50%
				padding 0 15px 30px 15px
				vertical-align top
