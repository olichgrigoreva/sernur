$(document).ready(function() {
	$('.recipe-page__img-item').on('click', function(){
		var thisItem = $(this).data('item');

		$('.recipe-page__img-item').removeClass('active');
		$('.recipe-page__img').removeClass('active');

		$(this).addClass('active');
		$('.recipe-page__img#' + thisItem).addClass('active');
	});

	$('.recipe-page__img').on('click', function(){
		var thisItem = $('.recipe-page__img-item.active').data('item');
		var firstItem = $('.recipe-page__img-item').eq(0).data('item');
		var lastItem = $('.recipe-page__img-item').last().data('item');

		$('.recipe-page__img-item').removeClass('active');
		$('.recipe-page__img').removeClass('active');

		if(!(thisItem == lastItem)) {
			$('.recipe-page__img-item[data-item=' + thisItem + ']').next().addClass('active');
			$('.recipe-page__img#' + thisItem).next().addClass('active');
		} else {
			$('.recipe-page__img-item[data-item=' + firstItem + ']').addClass('active');
			$('.recipe-page__img#' + firstItem).addClass('active');
		}

	});
});