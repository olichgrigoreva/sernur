class ChangePass {
	constructor() {
		this.selectors = {
			el: '.js-change-pass',
			form: '.js-change-pass-form',
			field: '.js-change-pass-field',
			btn: '.js-change-pass-btn'
		};

		this.classes = {
		};

		this.el = $(this.selectors.el);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors)
				.init();
		}
	}

	get({form, field, btn}) {
		this.elems = this.el.toArray().map((el) => {
			const $el = $(el);
			return {
				$el,
				$form: $el.find(form),
				$field: $el.find(field),
				$btn: $el.find(btn)
			}
		});
		return this;
	}

	init() {
		this.elems.forEach(el => {
			el.$btn.on('click', {that: this, el}, this.submit);
		});
	}

	submit(e) {
		e.preventDefault();

		const	{that, el} = e.data,
				pass = [];
		let 	valid = true;

		$(el.$field[1]).trigger('field:error', {show: false});

		el.$field.each((i, field) => {
			$(field).trigger('field:validate');
			if($(field).data('error') === true){
				valid = false;
			}
			pass[i] = $(field).find('.js-field-input').val();

		});


		if (valid && pass[0] !== pass[1]) {
			valid = false;
			$(el.$field[1]).trigger('field:error', {show: true, text: 'Пароли должны совпадать'});
		}

		if(valid) {
			el.$form.submit();
		}
	}
}
