class BlogPage {
	constructor() {
		this.selectors = {
			post: '.js-blog-page',
			picture: '.js-picture',
		};

		this.el = $(this.selectors.post);

		if (this.el.length) {
			this.init();
		}
	}

	init() {
		let pictures = Array.from(document.querySelectorAll(this.selectors.picture));

		pictures.forEach(picture => {
			if (picture.firstChild.naturalWidth < 800) {
				picture.classList.add('blog-page__picture_small');
			}
		});
	}
}
