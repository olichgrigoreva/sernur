class Bonuses {
	constructor() {
		this.selectors = {
			el: '.js-bonuses',
			total: '.js-total',
			hot: '.js-hot',
			btn: '.js-button',
			used: '.js-used'
		};

		this.classes = {
		};

		this.data = {
			total: 'total',
			percent: 'percent',
			possible: 'possible',
			eUse: 'event-use',
			eCartUpdate: 'event-cart-update'
		};

		this.el = $(this.selectors.el);
		this.$window = $(window);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data)
				.init();
		}
	}

	get({total, hot, btn, used}, data) {
		this.elems = this.el.toArray().map((el) => {
			const $el = $(el);
			return {
				$el,
				$total: $el.find(total),
				$hot: $el.find(hot),
				$btn: $el.find(btn),
				$used: $el.find(used),
				data: {
					eUse: $el.data(data.eUse) || 'cart:bonuses',
					total: $el.data(data.total),
					eCartUpdate: $el.data(data.eCartUpdate) || 'cart:update',
					percentOfSumm: $el.data(data.percent) || 30,
					possible: $el.data(data.possible) || 0,
					used: false
				}
			}
		});
		return this;
	}

	init() {
		this.elems.forEach(el => {
			el.$btn.on('click', {that: this, el}, this.use);

			this.$window.on(el.data.eCartUpdate, {that: this, el}, this.setBonuses);
		});
	}

	setBonuses(e, data) {
		const	{that, el} = e.data,
				{TotalItemPrice: summ} = data,
				{total, used, percentOfSumm: percent} = el.data;

		const	persOfSumm = (summ / 100 * percent).toFixed(),
				possible = total < persOfSumm ? total : persOfSumm;

		that.btnText(el);

		el.data.possible = possible;
		if(used) {
			el.$total.text(total - possible);
		} else {
			el.$total.text(total);
		}
		that.btnText(el);
	}

	btnText(el) {
		const 	{used, possible} = el.data,
				text = used ? 'Вернуть' : 'Использовать';

		el.$btn.text(`${text} ${possible} баллов`);
	}

	use(e) {
		e.preventDefault();
		const	{that, el} = e.data,
				{total} = el.data;
		let used = 0;

		if(!el.data.used) {
			used = el.data.possible
			el.$total.text(total - el.data.possible);
			el.data.used = true;
		} else {
			used = 0;
			el.$total.text(total);
			el.data.used = false;
		}

		el.$used.val(used);
		that.btnText(el);

		that.$window.trigger(el.data.eUse, {used});
	}
}
