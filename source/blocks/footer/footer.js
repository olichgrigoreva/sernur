class Footer {
	constructor() {
		this.selectors = {
			footer: '.js-footer'
		};

		this.classes = {
		};

		this.el = $(this.selectors.footer);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors)
				.init();
		}
	}

	get() {
		this.elems = this.el.toArray().map((el) => {
			const $el = $(el);
			return {
				$el
			}
		});
		return this;
	}

	init() {
		this.elems.forEach(elem => {
			// elem.$menuBtn.on('click', {that:this, elem}, this.mobileMenu);
		});
	}

	mobileSearch(e) {
		const {that, elem} = e.data;
		const {mobileSearch} = that.classes;

		elem.$el.addClass(mobileSearch);
	}
}
