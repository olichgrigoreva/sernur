class Scale {
	constructor() {
		this.selectors = {
			el: '.js-scale',
			value: '.js-scale-value',
			slider: '.js-scale-slider'
		};

		this.classes = {
		};

		this.data = {
			change: 'event-change'
		};

		this.el = $(this.selectors.el);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data)
				.init();
		}
	}

	get({value, slider}, {change}) {
		this.elems = this.el.toArray().map((el) => {
			const $el = $(el);
			return {
				$el,
				$value: $el.find(value),
				$slider: $el.find(slider),
				data: {
					change: $el.data(change) || 'scale:change'
				}
			}
		});
		return this;
	}

	init() {
		this.elems.forEach(el => {
			el.$el.on(el.data.change, {that:this, el}, this.change);
		});
	}

	change(e, data) {
		const	{that, el} = e.data,
				{total, value} = data;
		let perc = (value / total * 100);

		perc = perc > 100 ? '100%' : perc + '%';

		el.$value.css('width', perc);
		el.$slider.css('margin-left', perc);
	}
}
