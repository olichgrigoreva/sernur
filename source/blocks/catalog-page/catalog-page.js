class CatalogPage {
  constructor() {
    this.selectors = {
      el: ".js-catalog-page",
      grid: ".js-catolog-grid",
      loader: ".js-catalog-loader",
      seo: ".js-catalog-seo"
    };

    this.classes = {
      loader: ".catalog-page__loader_show"
    };

    this.data = {};

    this.el = $(this.selectors.el);
    this.$window = $(window);

    if (this.el.length) {
      this.elems = [];
      this.get(this.selectors, this.data).init();
    }
  }

  get({ slider, fat, seo, fields, form, grid, loader }, data) {
    this.elems = this.el.toArray().map(el => {
      const $el = $(el);
      return {
        $el,
        $slider: $el.find(slider),
        $fat: $el.find(fat),
        $grid: $el.find(grid),
        $loader: $el.find(loader),
        $seo: $el.find(seo),
        data: {}
      };
    });
    return this;
  }

  init() {
    this.elems.forEach(el => {
      this.$window.on(
        "menu:filterChange",
        { that: this, el },
        this.getProducts
      );
      el.$seo.find(".catalog-page__seo-collapsible").click(function() {
        el.$seo.find(".catalog-page__seo-details").toggle();
      });
    });
  }

  getProducts(e, formData) {
    const { that, el } = e.data,
      callback = that.setProducts,
      context = { that, el };

    let data = "";

    history.pushState(
      null,
      null,
      window.location.origin + window.location.pathname + "?" + formData.form
    );

    if (formData.params !== "") {
      data = formData.form + "&" + formData.params;
    } else {
      data = formData.form;
    }

    el.$loader.addClass(that.classes.loader);

    that.$window.trigger("ajax:get", {
      url: formData.url,
      dataType: "HTML",
      data,
      callback,
      context
    });
  }

  setProducts(data) {
    const { that, el } = data.context;

    el.$grid.find("*").remove();
    el.$grid.append(data.data);
    el.$loader.removeClass(that.classes.loader);

    that.$window.trigger("product:reload");
  }
}
