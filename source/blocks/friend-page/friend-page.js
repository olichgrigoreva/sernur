class FriendPage {
	constructor() {
		this.selectors = {
			el: '.js-friend-page',
			copy: '.js-friend-page-copy',
			copyBtn: '.js-friend-page-copy-btn',
			form: '.friend-page__form',
			invite: '.js-friend-page-invite',
			inviteBtn: '.js-friend-page-invite-btn',
			inviteInfo: '.js-friend-page-invite-info'
		};

		this.classes = {
			formInfo: 'friend-page__form_info'
		}

		this.data = {
		}

		this.el = $(this.selectors.el);
		this.$window = $(window);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data)
				.init();
		}
	}

	get({copy, copyBtn, invite, inviteBtn, inviteInfo}, data) {
		const {eventGoodsAdd} = data;

		this.elems = this.el.toArray().map((el) => {
			const $el = $(el);
			return {
				$el,
				$copy: $el.find(copy),
				$copyBtn: $el.find(copyBtn),
				$invite: $el.find(invite),
				$inviteBtn: $el.find(inviteBtn),
				$inviteInfo: $el.find(inviteInfo),
				data: {
				}
			}
		});
		return this;
	}

	init() {
		this.elems.forEach(el => {
			el.$inviteBtn
				.on('click', {that: this, el}, this.invite);
			el.$copyBtn
				.on('click', {that: this, el}, this.copyClick);
			el.$copy
				.on('field:copied', {that: this, el}, this.showInfo);
		});
	}

	invite(e) {
		e.preventDefault();

		const 	{el, that} = e.data,
				{form} = that.selectors,
				$form = $(e.currentTarget).parents(form),
				data = $form.serialize(),
				url = $form.prop('action'),
				callback = that.inviteSended;

		el.$invite.trigger('field:validate');
		if (el.$invite.data('error') === false) {
			that.$window.trigger('ajax:get', {url, data, callback, context: {that, el, target: e.currentTarget}});
		}
	}

	inviteSended(e) {
		const 	{el, that, target} = e.context,
				{message} = e.data;

		el.$inviteInfo.text(message);
		that.showInfo({data: {that, el}, currentTarget: target});

	}

	copyClick(e) {
		const {el, that} = e.data;

		el.$copy.trigger('field:copy');
	}

	showInfo(e) {
		const 	{el, that} = e.data,
				{form} = that.selectors,
				{formInfo} = that.classes,
				$form = $(e.currentTarget).parents(form);

		$form.addClass(formInfo);

		setTimeout(function(){
                that.hideInfo(el, $form);
            }.bind(that), 2000);
	}

	hideInfo(el, $form) {
		const 	{formInfo} = this.classes;

		$form.removeClass(formInfo);
	}
}
