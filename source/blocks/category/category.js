class Category {
	constructor() {
		this.selectors = {
			el: '.js-category',
			btn: '.js-category-btn'
		};

		this.classes = {
			show: 'category_show'
		};

		this.data = {
		};
		this.el = $(this.selectors.el);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data)
				.init();
		}
	}

	get({btn}, {}) {
		this.elems = this.el.toArray().map((el) => {
			const $el = $(el);
			return {
				$el,
				$btn: $el.find(btn),
				data: {
				}
			};
		});
		return this;
	}

	init() {
		this.elems.forEach(el => {
			el.$btn.on('click', {that:this, el}, this.showMore);
		});
	}

	showMore(e) {
		const	{that, el} = e.data,
				{show} = that.classes;

		el.$el.addClass(show);
	}
}
