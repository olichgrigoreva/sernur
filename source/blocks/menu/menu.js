class Menu {
	constructor() {
		this.selectors = {
			el: '.js-menu',
			slider: '.js-slider',
			fat: '.js-menu-fat-val',
			form: '.js-menu-form',
			fields: '.js-field',

			sub: '.js-menu-sub',
			subItem: '.js-menu-sub-item',
			subHidden: '.js-menu-sub-hidden',
			subHiddenWrap: '.js-menu-sub-hidden-wrap',

			filterSlider: '.js-menu-filter-slider',
			filterItem: '.js-menu-filter-item',
			filterHidden: '.js-menu-filter-hidden',
			filterHiddenWrap: '.js-menu-filter-hidden-wrap',

			topMore: '.js-menu-top-more'
		};


		this.classes = {
			subTrim: 'menu__sub_trim',
			subItemHide: 'menu__sub-item_hide',

			filterTrim: 'menu__filter_trim',
			filterItemHide: 'menu__filter-check-el_hide',

			short: 'menu_short'
		};

		this.data = {
		};

		this.el = $(this.selectors.el);
		this.$window = $(window);

		this.desktopWidth = 1000;

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors)
				.init();
		}
	}

	get(sel) {
		this.elems = this.el.toArray().map((el) => {
			const $el = $(el);
			return {
				$el,
				$slider: $el.find(sel.slider),
				$fat: $el.find(sel.fat),
				$form: $el.find(sel.form),
				$fields: $el.find(sel.fields),

				$sub: $el.find(sel.sub),
				$subItems: $el.find(sel.subItem),
				$subHidden: $el.find(sel.subHidden),
				$subHiddenWrap: $el.find(sel.subHiddenWrap),

				$filterItems: $el.find(sel.filterItem),
				$filterSlider: $el.find(sel.filterSlider),
				$filterHidden: $el.find(sel.filterHidden),
				$filterHiddenWrap: $el.find(sel.filterHiddenWrap),

				$topMore: $el.find(sel.topMore),

				data: {
					subParams: {
						items: [],
						hiddenWrap: 0
					},
					filterParams: {
						items: [],
						hiddenWrap: 0,
						slider: 0
					}
				}
			}
		});
		return this;
	}

	init() {
		this.elems.forEach(el => {
			el.$slider
				.on('slider:update', {that: this, el}, this.changeFat)
				.on('slider:change', {that: this, el}, this.change);
			el.$fields.on('field:change', {that: this, el}, this.change);

			el.$topMore.on('click', {that: this, el}, this.showTopItems);

			this.$window.on('resize', {that: this, el}, this.runOverflowCheck);

			this.getOverflowElsParams(el);
			this.runOverflowCheck({data: {el, that: this}});
			// Stickyfill.add(el.$el);
		});
	}

	showTopItems(e) {
		const	{that, el} = e.data,
				{short} = that.classes;

		el.$el.removeClass(short);
	}

	getOverflowElsParams(el) {
		el.$subItems.each((i, item) => {
			el.data.subParams.items.push(item.offsetWidth);
		});
		el.data.subParams.hiddenWrap = el.$subHidden.innerWidth() + 50;

		el.$filterItems.each((i, item) => {
			el.data.filterParams.items.push($(item).innerWidth());
		});
		el.data.filterParams.hiddenWrap = el.$filterHidden.innerWidth() + 60;
		el.data.filterParams.slider = el.$filterSlider.innerWidth();
	}

	runOverflowCheck(e) {
		const	{that, el} = e.data,
				{classes} = that;

		if(!window.innerWidth < that.desktopWidth) {
			that.checkOverflow(el, el.data.subParams, el.$subItems, el.$sub, el.$subHiddenWrap, {trim: classes.subTrim, itemHide: classes.subItemHide});

			that.checkOverflow(el, el.data.filterParams, el.$filterItems, el.$form, el.$filterHiddenWrap, {trim: classes.filterTrim, itemHide: classes.filterItemHide});
		}
	}

	checkOverflow(el, params, items, wrap, hiddenWrap, {trim, itemHide}) {
		const	wrapWidth = wrap.innerWidth(),
				slider = params.slider ? params.slider : 0;

		let 	totalWidth = 0,
				overflow = false;

		hiddenWrap.children().remove();
		items.removeClass(itemHide);

		items.each( (i, item) => {
			totalWidth += params.items[i];

			if(totalWidth + params.hiddenWrap + slider >= wrapWidth) {
				overflow = true;
				if(slider) {
					let clone = $(item).clone(true);
					clone.find('.js-field-input').prop('id', 'temp'+ i);
					clone.find('.js-field-label').prop('for', 'temp'+ i);
					clone.appendTo(hiddenWrap);
				}
				else {
					$(item).clone().appendTo(hiddenWrap);
				}
				$(item).addClass(itemHide);
			}
		});

		if(slider) {
			hiddenWrap.find('.js-field').each((i, item) => {
				this.$window.trigger('field:add', {el: $(item)});
			});
		}

		wrap.toggleClass(trim, overflow);
	}

	changeFat(e, data) {
		const	{that, el} = e.data,
				{start, end} = data,
				{$fat} = el;

		$fat.text(`${start}..${end} %`);
	}

	change(e) {
		const	{that, el} = e.data,
				{$form} = el,
				formData = $form.data(),
				data = {
					form: $form.serialize(),
					params: $.param(formData),
					url: $form.prop('action')
				};

		that.$window.trigger('menu:filterChange', data);
	}
}
