class CartItem {
	constructor() {
		this.selectors = {
			el: '.js-cart-item',
			counter: '.js-counter',
			del: '.js-delete',
			return: '.js-return',
			add: '.js-add',
			price: '.js-price',
			form: '.js-cart-item-form'
		};

		this.classes = {
			deleted: 'cart-item_deleted',
			offer: 'cart-item_offer'
		};

		this.data = {
			id: 'id',
			price: 'item-price',
			weight: 'item-weight',
			amount: 'item-amount',
			eventChange: 'event-change',
			eventAdd: 'event-add',
			eventGet: 'event-get'
		}

		this.el = $(this.selectors.el);
		this.$window = $(window);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data)
				.init();
		}
	}

	get({counter, del, return: ret, add, price, form}, data) {
		const 	{id, price: dataPrice, weight, amount, eventChange, eventAdd, eventGet} = data;

		this.elems = this.el.toArray().map((el) => {
			const $el = $(el);
			return {
				$el,
				$counter: $el.find(counter),
				$del: $el.find(del),
				$return: $el.find(ret),
				$add: $el.find(add),
				$price: $el.find(price),
				$form: $el.find(form),
				data: {
					id: $el.data(id),
					price: $el.data(dataPrice),
					weight: $el.data(weight) || false,
					amount: $el.data(amount),
					eventChange: $el.data(eventChange) || 'cartItem:change',
					eventAdd: $el.data(eventAdd) || 'cartItem:add',
					eventGet: $el.data(eventGet) || 'cartItem:get',
					isDeleted: $el.hasClass(this.classes.deleted)
				}
			}
		});
		return this;
	}

	init() {
		this.elems.forEach(el => {
			el.$del.on('click', {that: this, el}, this.deleteItem);
			el.$return.on('click', {that: this, el}, this.returnItem);
			el.$add.on('click', {that: this, el}, this.addItem);
			el.$counter.on('counter:change', {that: this, el}, this.amountChange);
			el.$el.on(el.data.eventGet, {el}, this.getItemData);
		});
	}

	amountChange(e, data) {
		const	{that, el} = e.data,
				{eventChange, id, price, weight} = el.data,
				{value, result} = data,
				formData = $.param(el.$form.data()) + '&' + el.$form.serialize(),
				url = el.$form.prop('action'),
				rate = 1000;

		el.data.amount = value;
		let summ = ((value * rate) * (price * rate)) / (rate * rate),
			mass = weight ? (weight * value) : value;

		el.$price.text(summ.toString().replace('.', ','));

		el.$el.trigger(el.data.eventChange, {id, amount: value, summ, mass});

		that.$window.trigger('ajax:get', {url, data: formData, callback: that.cartСontents, context: that});
	}

	addItem(e) {
		const	{el, that} = e.data,
				{eventChange, id} = el.data;

		el.$el.removeClass(that.classes.offer);
		el.$counter.trigger('counter:get');
		el.$el.trigger(el.data.eventAdd);
	}

	deleteItem(e) {
		const	{el, that} = e.data,
				{eventChange, id} = el.data,
				idName = el.$counter.find('input').prop('name'),
				formData = $.param(el.$form.data()) + '&' + idName + '=0',
				url = el.$form.prop('action');

		el.data.isDeleted = true;
		el.$el.addClass(that.classes.deleted);
		el.$el.trigger(el.data.eventChange, {id, amount: 0, summ: 0, mass: 0});

		that.$window.trigger('ajax:get', {url, data: formData, callback: that.cartСontents, context: that});
	}

	returnItem(e) {
		const	{el, that} = e.data;

		el.isDeleted = false;
		el.$el.removeClass(that.classes.deleted);
		el.$counter.trigger('counter:get');
	}

	getItemData(e) {
		const	{el} = e.data;

		el.$counter.trigger('counter:get');
	}

	cartСontents({data, context: that}) {
		that.$window.trigger('cart:update', data);

	}
}
