class Promo {
	constructor() {
		this.selectors = {
			el: '.js-promo',
			add: '.js-promo-add',
			field: '.js-field',
			wrap: '.js-promo-wrap',
			fieldWrap: '.js-promo-field'
		};

		this.classes = {
			active: 'promo_active'
		};

		this.data = {
			url: 'validate-url',
			eventValid: 'event-valid'
		};

		this.el = $(this.selectors.el);
		this.$window = $(window);

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data)
				.init();
		}
	}

	get({add, field, wrap, fieldWrap}, {url, eventValid}) {
		this.elems = this.el.toArray().map((el) => {
			const $el = $(el);
			return {
				$el,
				$field: $el.find(field),
				$add: $el.find(add),
				$wrap: $el.find(wrap),
				$fieldWrap: $el.find(fieldWrap),
				data: {
					id: 0,
					url: $el.data(url),
					eventValid: $el.data(eventValid) || 'cart:promo',
					email: false
				}
			}
		});
		return this;
	}

	init() {
		this.elems.forEach(el => {
			this.$window.on('cart:email', {that: this, el}, this.setEmail);
			el.$add.on('click', {that: this, el}, this.add);
		});
	}

	setEmail(e, data) {
		const	{that, el} = e.data,
				{email} = data;

		el.data.email = email;
	}

	add(e) {
		e.preventDefault();
		const	{that, el} = e.data,
				{active, idPref} = that.classes,
				{field} = that.selectors,
				id = el.data.id += 1;

		el.$el.addClass(active);

		let $newEl = el.$field.clone();
		el.$wrap.append($newEl);


		$newEl
			.data('id', id)
			.find('.field__label').text(`Промокод #${ id }`);

		that.$window.trigger('field:add', {el: $newEl});
		$newEl
			.on('field:input', {that, el, target: $newEl}, that.change)
			.on('field:clear', {that, el, target: $newEl}, that.hideFields)
			.find('.js-field-input')
				.prop('disabled', false)
				.prop('id', `promo${ id }`);

	}

	hideFields(e) {
		const	{that, el} = e.data,
				{eventValid} = el.data,
				$cur = $(e.currentTarget),
				{active} = that.classes,
				id = $cur.data('id');

		that.$window.trigger(eventValid, {id, remove: true});
		$cur.remove();
		el.$el.toggleClass(active, el.$wrap.children().length > 1);
	}

	change(e, data) {
		const	{that, el, target} = e.data,
				{value} = data;

		if(el.data.timer){
			clearTimeout(el.data.timer);
		}
		that.$window.trigger('cart:getEmail');
		el.data.timer = setTimeout(that.validatePromo, 2000, {that, target, el, value});
	}

	validatePromo(data) {
		const	{that, el, target, value} = data,
				{url} = el.data,
				callback = that.setAnswer,
				context = {that, el, target},
				field = target.find('.js-field-input');
		let formData = {};

		if(el.data.email) {
			field.trigger('field:extra', {
				show: false
			});
			formData = {
				email: el.data.email
			};
			formData[field.prop('name')] = field.val();

			that.$window.trigger('ajax:get', {url, data: formData, callback, context})
		}
		else {
			field.trigger('field:extra', {
				text: 'Введите электроную почту и повторите ввод',
				show: true
			});
		}
	}

	setAnswer(data) {
		const	{that, el, target} = data.context,
				{eventValid} = el.data,
				id = target.data('id'),
				{error, valid, type, value, percent, message, error_message} = data.data;

		if(error !== undefined || valid === false) {
			const text = error_message || 'Неверный промокод';

			let data = {
				show: true,
				text
			};
			target.trigger('field:error', data);
		}
		else {
			let data = {
				show: true,
				text: message || 'Верный промокод'
			};

			target
				.trigger('field:disable', {disabled: true})
				.trigger('field:extra', data);
			that.$window.trigger(eventValid, {type, value, percent, id, message});
		}
	}
}
