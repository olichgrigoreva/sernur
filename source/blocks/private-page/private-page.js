class PrivatePage {
	constructor() {
		this.selectors = {
			el: '.js-private-page',
			form: '.js-private-page-form',
			resume: '.js-private-page-resume',
			changes: '.js-private-page-changes',
			saveBtn: '.js-private-page-save-btn',
			infoFields: '.js-private-page-info .js-field',
			addr: '.js-saved-addr',
			history: '.js-private-page-history',
			historyHide: '.js-private-page-history-hide',

			footer: '.js-footer'
		};

		this.classes = {
			fixed: 'private-page__resume_fixed',
			changes: 'private-page_changes',
			historyHide: 'private-page__history_hide'
		};

		this.data = {
		}

		this.el = $(this.selectors.el);
		this.$window = $(window);
		this.$document = $(document);
		this.$footer = this.$document.find(this.selectors.footer);

		this.desktopWidth = 1000;

		if (this.el.length) {
			this.elems = [];
			this.get(this.selectors, this.data)
				.init();
		}
	}

	get({form, resume, changes, saveBtn, infoFields, addr, history, historyHide}, data) {
		this.elems = this.el.toArray().map((el) => {
			const $el = $(el);
			return {
				$el,
				$form: $el.find(form),
				$resume: $el.find(resume),
				$changes: $el.find(changes),
				$saveBtn: $el.find(saveBtn),
				$infoFields: $el.find(infoFields),
				$addr: $el.find(addr),
				$history: $el.find(history),
				$historyBtn: $el.find(historyHide),
				data: {
					changedEls: {}
				}
			}
		});
		return this;
	}

	init() {
		this.elems.forEach(el => {
			this.setResumePosition({data: {that: this, el}});

			// this.$window
				// .on('resize, scroll', {that: this, el}, this.setResumePosition);

			el.$infoFields.on('field:change', {that: this, el}, this.infoChange);
			el.$addr.on('savedAddr:delete', {that: this, el}, this.infoChange);
			el.$historyBtn.on('click', {that: this, el}, this.hideHistory);
			el.$saveBtn.on('click', {el}, this.sendForm);
		});
	}

	sendForm(e) {
		const	{el} = e.data;

		el.$form.submit();
	}

	hideHistory(e) {
		const	{that, el} = e.data,
		{historyHide} = that.classes;

		el.$history.toggleClass(historyHide);
	}

	infoChange(e, data) {
		const	{that, el} = e.data,
				{value, label, addr} = data;

		el.data.changedEls[label] = value;
		that.showChanges(el);
	}

	showChanges(el) {
		const {changes} = this.classes,
		{changedEls} = el.data;

		if(!jQuery.isEmptyObject(changedEls)){
			let text = '';
			for (let key in changedEls) {
				text += `${key} – ${changedEls[key]}, `;
			}

			el.$changes.text(text.slice(0, -2));
			el.$el.addClass(changes);

			Stickyfill.add(el.$resume);
		}
	}

	setResumePosition(e) {
		const	{that, el} = e.data,
				{fixed} = that.classes,
				isMobile = that.$window.width() < that.desktopWidth;

		if(isMobile){
			el.$resume.removeClass(fixed);
		}
		else {
			const 	elHeigth = el.$resume.height(),
					dHeight = that.$document.height(),
					wHeight = that.$window.height(),
					scrollPos = that.$document.scrollTop(),
					footerH = that.$footer.height(),

					pos = (dHeight - wHeight) - scrollPos - footerH,
					hide = pos > elHeigth;

			el.$resume.toggleClass(fixed, hide);
		}
	}
}
